pub(crate) mod util;

#[cfg(not(feature = "multithreaded"))]
pub(crate) mod st;

#[cfg(feature = "multithreaded")]
pub(crate) mod mt;

#[cfg(not(feature = "multithreaded"))]
pub use st::WaefEcsRegistry;

#[cfg(feature = "multithreaded")]
pub use mt::WaefEcsRegistry;

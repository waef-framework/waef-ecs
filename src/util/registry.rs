#[cfg(not(feature = "multithreaded"))]
pub(crate) mod st;

#[cfg(feature = "multithreaded")]
pub(crate) mod mt;

#[cfg(not(feature = "multithreaded"))]
pub use st::ArchetypeRegistry;

#[cfg(feature = "multithreaded")]
pub use mt::ArchetypeRegistry;

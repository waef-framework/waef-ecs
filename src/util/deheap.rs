use std::ops::{Index, IndexMut};

use waef_core::WaefError;

#[derive(Clone)]
#[allow(unused)]
pub struct Deheap {
    data: Vec<u8>,
    item_size: usize,
    length: usize,
}
#[allow(unused)]
impl Deheap {
    pub fn with_capacity(item_size: usize, capacity: usize) -> Self {
        Self {
            data: Vec::with_capacity(item_size * capacity),
            item_size,
            length: 0,
        }
    }

    pub fn push(&mut self, data: &[u8]) {
        if data.len() != self.item_size {
            panic!(
                "Request was made to push item of size {} onto PodVec with item size {}",
                data.len(),
                self.item_size
            );
        }
        self.push_unsafe(data);
    }

    pub fn push_unsafe(&mut self, data: &[u8]) {
        self.data.extend(data);
        self.length += 1;
    }

    pub fn try_push(&mut self, data: &[u8]) -> Result<(), WaefError> {
        if data.len() != self.item_size {
            Err(WaefError::new(format!(
                "Request was made to push item of size {} onto PodVec with item size {}",
                data.len(),
                self.item_size
            )))
        } else {
            self.push_unsafe(data);
            Ok(())
        }
    }

    pub fn pop(&mut self) -> Option<Vec<u8>> {
        if self.data.len() < self.item_size {
            None
        } else {
            let removed = self.data.split_off(self.data.len() - self.item_size);
            if removed.is_empty() {
                None
            } else {
                self.length -= 1;
                Some(removed)
            }
        }
    }

    pub fn pop_front(&mut self) -> Option<Vec<u8>> {
        if self.data.len() < self.item_size {
            None
        } else {
            let mut remaining = self.data.split_off(self.item_size);
            std::mem::swap(&mut remaining, &mut self.data);

            let removed = remaining;
            if removed.is_empty() {
                None
            } else {
                self.length -= 1;
                Some(removed)
            }
        }
    }

    pub fn len(&self) -> usize {
        self.length
    }

    pub fn item_len(&self) -> usize {
        self.item_size
    }

    pub fn iter_ptr_mut(&mut self) -> impl Iterator<Item = *mut u8> + '_ {
        self.data[..]
            .chunks(self.item_size)
            .map(|x: &[u8]| x.as_ptr() as *mut _)
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut [u8]> + '_ {
        self.data[..]
            .chunks(self.item_size)
            .map(|x: &[u8]| x.as_ptr() as *mut _)
            .map(|x| unsafe { std::slice::from_raw_parts_mut(x, self.item_size) })
    }

    pub fn iter(&self) -> impl Iterator<Item = &[u8]> + '_ {
        self.data[..].windows(self.item_size)
    }
}
impl Index<usize> for Deheap {
    type Output = [u8];

    fn index(&self, index: usize) -> &Self::Output {
        let head = index * self.item_size;
        &self.data[head..head + self.item_size]
    }
}
impl IndexMut<usize> for Deheap {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        let head = index * self.item_size;
        &mut self.data[head..head + self.item_size]
    }
}

#[cfg(test)]
pub mod tests {
    use super::Deheap;

    #[test]
    pub fn pod_vec_tests() {
        let mut data = Deheap::with_capacity(16, 32);
        assert_eq!(0, data.len());
        assert!(data.pop().is_none());
        assert!(data.pop_front().is_none());

        data.push(&u128::to_le_bytes(100));
        data.push(&u128::to_le_bytes(200));
        data.push(&u128::to_le_bytes(300));
        data.push(&u128::to_le_bytes(400));
        assert_eq!(4, data.len());

        assert_eq!(
            400,
            u128::from_le_bytes(data.pop().unwrap().try_into().unwrap_or_default())
        );
        assert_eq!(3, data.len());

        assert!(data.try_push(&u64::to_le_bytes(400)).is_err());
        assert_eq!(3, data.len());

        assert_eq!(
            100,
            u128::from_le_bytes(data.pop_front().unwrap().try_into().unwrap_or_default())
        );

        assert_eq!(
            200,
            u128::from_le_bytes(data[0].try_into().unwrap_or_default())
        );
        assert_eq!(
            300,
            u128::from_le_bytes(data[1].try_into().unwrap_or_default())
        );
    }
}

use std::{
    cell::RefCell,
    collections::{BTreeSet, HashMap},
    fmt::Debug,
    hash::Hash,
};

use parking_lot::{ReentrantMutex, RwLock};
use waef_core::WaefError;

use super::super::archetype_store::ArchetypeStore;

#[derive(Clone, PartialEq, Eq)]
struct ArchetypeKey<ComponentKey: Eq + Hash + Ord + Clone>(pub BTreeSet<ComponentKey>);

impl<ComponentKey: Eq + Hash + Ord + Clone> Hash for ArchetypeKey<ComponentKey> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.len().hash(state);
        self.0
            .iter()
            .for_each(|component_name| component_name.hash(state));
    }
}
impl<ComponentKey: Eq + Hash + Ord + Clone> ArchetypeKey<ComponentKey> {
    pub fn contains(&self, subset: &BTreeSet<ComponentKey>) -> bool {
        self.0.intersection(subset).count() == subset.len()
    }

    pub fn excludes(&self, subset: &BTreeSet<ComponentKey>) -> bool {
        self.0.intersection(subset).count() == 0
    }
}
impl<ComponentKey: Eq + Hash + Ord + Clone + Debug> Debug for ArchetypeKey<ComponentKey> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

pub struct ArchetypeRegistry<ComponentKey: Eq + Hash + Ord + Clone> {
    archetype_capacity: usize,
    archetypes: RwLock<
        HashMap<ArchetypeKey<ComponentKey>, ReentrantMutex<RefCell<ArchetypeStore<ComponentKey>>>>,
    >,
    entity_archetypes: RwLock<HashMap<u128, ArchetypeKey<ComponentKey>>>,
}
#[allow(unused)]
impl<'a, ComponentKey: Eq + Hash + Clone + Ord + Debug> ArchetypeRegistry<ComponentKey> {
    pub fn with_capacity(
        entity_capacity: usize,
        component_capacity: usize,
        archetype_capacity: usize,
    ) -> Self {
        Self {
            archetype_capacity,
            entity_archetypes: RwLock::new(HashMap::with_capacity(entity_capacity)),
            archetypes: RwLock::new(HashMap::with_capacity(component_capacity)),
        }
    }

    pub fn insert_entity(&self, entity_id: u128, components: HashMap<ComponentKey, Box<[u8]>>) {
        let _span =
            tracing::trace_span!("ecs_archetype_registry::insert_entity", entity_id).entered();

        let (mut entity_archetypes, mut archetypes) = {
            let _guard = tracing::trace_span!("ecs_archetype_registry::write_locks").entered();
            (self.entity_archetypes.write(), self.archetypes.write())
        };

        self.unsafe_insert_entity(components, entity_archetypes, entity_id, archetypes)
    }

    fn unsafe_insert_entity(
        &self,
        components: HashMap<ComponentKey, Box<[u8]>>,
        mut entity_archetypes: parking_lot::lock_api::RwLockWriteGuard<
            parking_lot::RawRwLock,
            HashMap<u128, ArchetypeKey<ComponentKey>>,
        >,
        entity_id: u128,
        mut archetypes: parking_lot::lock_api::RwLockWriteGuard<
            parking_lot::RawRwLock,
            HashMap<
                ArchetypeKey<ComponentKey>,
                ReentrantMutex<RefCell<ArchetypeStore<ComponentKey>>>,
            >,
        >,
    ) {
        let _span = tracing::trace_span!("ecs_archetype_registry::unsafe_insert_entity", entity_id)
            .entered();
        if components.is_empty() {
            entity_archetypes.insert(entity_id, ArchetypeKey(BTreeSet::new()));
            return;
        }

        let component_keys: BTreeSet<_> = components.keys().cloned().collect();
        let archetype_key = ArchetypeKey(component_keys);

        if let Some(archetype_store_lock) = archetypes.get_mut(&archetype_key) {
            let _span = tracing::trace_span!(
                "ecs_archetype_registry::existing_store::insert_entity",
                entity_id
            )
            .entered();

            let mut archetype_store = {
                let _guard = tracing::trace_span!("ecs_archetype_registry::store_lock").entered();
                archetype_store_lock.lock()
            };
            archetype_store.borrow_mut().insert(entity_id, components);
            entity_archetypes.insert(entity_id, archetype_key);
        } else {
            let _span = tracing::trace_span!(
                "ecs_archetype_registry::new_store::insert_entity",
                entity_id
            )
            .entered();

            let component_definition: HashMap<ComponentKey, usize> = components
                .iter()
                .map(|(key, component)| (key.clone(), component.len()))
                .collect();

            let mut archetype_store =
                ArchetypeStore::with_capacity(self.archetype_capacity, component_definition);
            archetype_store.insert(entity_id, components);
            archetypes.insert(
                archetype_key.clone(),
                ReentrantMutex::new(RefCell::new(archetype_store)),
            );
            entity_archetypes.insert(entity_id, archetype_key);
        }
    }

    pub fn add_component_to_entity(
        &self,
        entity_id: u128,
        component: (ComponentKey, Box<[u8]>),
    ) -> Result<(), WaefError> {
        let _span =
            tracing::trace_span!("ecs_archetype_registry::add_component_to_entity", entity_id)
                .entered();

        let (mut entity_archetypes, mut archetypes) = {
            let _guard = tracing::trace_span!("ecs_archetype_registry::write_locks").entered();
            (self.entity_archetypes.write(), self.archetypes.write())
        };

        if let Some(mut components) =
            Self::unsafe_remove_entity(&mut entity_archetypes, entity_id, &mut archetypes)
        {
            components.insert(component.0, component.1);
            self.unsafe_insert_entity(components, entity_archetypes, entity_id, archetypes);
            Ok(())
        } else {
            Err(WaefError::new("entity not found when adding component"))
        }
    }

    pub fn remove_component_from_entity(
        &self,
        entity_id: u128,
        component: ComponentKey,
    ) -> Result<(), WaefError> {
        let _span = tracing::trace_span!(
            "ecs_archetype_registry::remove_component_from_entity",
            entity_id
        )
        .entered();

        let (mut entity_archetypes, mut archetypes) = {
            let _guard = tracing::trace_span!("ecs_archetype_registry::write_locks").entered();
            (self.entity_archetypes.write(), self.archetypes.write())
        };

        if let Some(mut components) =
            Self::unsafe_remove_entity(&mut entity_archetypes, entity_id, &mut archetypes)
        {
            components.remove(&component);
            self.unsafe_insert_entity(components, entity_archetypes, entity_id, archetypes);
            Ok(())
        } else {
            Err(WaefError::new("entity not found when removing component"))
        }
    }

    pub fn remove_entity(&self, entity_id: u128) -> Option<HashMap<ComponentKey, Box<[u8]>>> {
        let (mut entity_archetypes, mut archetypes) = {
            let _guard = tracing::trace_span!("ecs_archetype_registry::write_locks").entered();
            (self.entity_archetypes.write(), self.archetypes.write())
        };

        Self::unsafe_remove_entity(&mut entity_archetypes, entity_id, &mut archetypes)
    }

    pub fn query(
        &self,
        component_includes: BTreeSet<ComponentKey>,
        component_excludes: BTreeSet<ComponentKey>,
        mut callback: impl FnMut(&ArchetypeStore<ComponentKey>),
    ) {
        let (entity_archetypes, archetypes) = {
            let _guard =
                tracing::trace_span!("ecs_archetype_registry::read_recursive_locks").entered();
            (
                self.entity_archetypes.read_recursive(),
                self.archetypes.read_recursive(),
            )
        };

        archetypes
            .iter()
            .for_each(&mut |(archetype_components, archetype_store_lock): (
                &ArchetypeKey<ComponentKey>,
                &ReentrantMutex<RefCell<ArchetypeStore<ComponentKey>>>,
            )| {
                if archetype_components.contains(&component_includes)
                    && archetype_components.excludes(&component_excludes)
                {
                    let _guard = tracing::trace_span!("ecs_archetype_registry::query_store", store_key = ?archetype_components).entered();

                    let store = {
                        let _guard =
                            tracing::trace_span!("ecs_archetype_registry::store_lock").entered();
                        archetype_store_lock.lock()
                    };
                    callback(&store.borrow());
                }
            })
    }

    fn unsafe_remove_entity(
        entity_archetypes: &mut parking_lot::lock_api::RwLockWriteGuard<
            parking_lot::RawRwLock,
            HashMap<u128, ArchetypeKey<ComponentKey>>,
        >,
        entity_id: u128,
        archetypes: &mut parking_lot::lock_api::RwLockWriteGuard<
            parking_lot::RawRwLock,
            HashMap<
                ArchetypeKey<ComponentKey>,
                ReentrantMutex<RefCell<ArchetypeStore<ComponentKey>>>,
            >,
        >,
    ) -> Option<HashMap<ComponentKey, Box<[u8]>>> {
        let _span = tracing::trace_span!("ecs_archetype_registry::unsafe_remove_entity", entity_id)
            .entered();

        if let Some(archetype_key) = entity_archetypes.remove(&entity_id) {
            if !archetype_key.0.is_empty() {
                let _span = tracing::trace_span!(
                    "ecs_archetype_registry::unsafe_remove_entity_from_store",
                    entity_id
                )
                .entered();
                if let Some(archetype_store_lock) = archetypes.get_mut(&archetype_key) {
                    let mut archetype_store = {
                        let _guard =
                            tracing::trace_span!("ecs_archetype_registry::store_lock").entered();
                        archetype_store_lock.lock()
                    };
                    let result = archetype_store.borrow_mut().remove(entity_id);
                    if archetype_store.borrow().is_empty() {
                        drop(archetype_store);
                        archetypes.remove(&archetype_key);
                    }
                    match result {
                        Some(result) => Some(result),
                        None => {
                            tracing::error!("Entity Archetype for entity {} contained {} archetypes but could not find entity in the archetype store", entity_id, archetype_key.0.len());
                            None
                        }
                    }
                } else {
                    tracing::error!("Entity Archetype for entity {} contained {} archetypes but could not find archetype store", entity_id, archetype_key.0.len());
                    Some(HashMap::new())
                }
            } else {
                Some(HashMap::new())
            }
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use core::panic;

    use super::ArchetypeRegistry;
    use waef_core::Pod;

    #[test]
    pub fn archetype_registry_single_test() {
        let registry = ArchetypeRegistry::<u32>::with_capacity(1048576, 1024, 32);

        let entity_id = 3515135135135135135u128;
        registry.insert_entity(
            entity_id,
            [
                (
                    32,
                    u32::as_bytes(&0xFFBB0065u32).to_vec().into_boxed_slice(),
                ),
                (8, u8::as_bytes(&0x65u8).to_vec().into_boxed_slice()),
            ]
            .into(),
        );

        let mut invocation_count = 0;
        registry.query([32].into(), [].into(), |archetype| {
            let component_indices: Vec<_> =
                archetype.component_heap_indices([32, 8].iter()).collect();

            assert_eq!(2, component_indices.len());
            assert!(component_indices[0].is_some());
            assert!(component_indices[1].is_some());

            let u32_component =
                unsafe { archetype.component_heap_at_index_unsafe(component_indices[0].unwrap()) };
            let u8_component =
                unsafe { archetype.component_heap_at_index_unsafe(component_indices[1].unwrap()) };

            archetype.entity_heap_indices().for_each(|entity_index| {
                assert_eq!(
                    3515135135135135135u128,
                    archetype.entity_id_at_index(entity_index)
                );

                let u32_component = u32::from_bytes(&u32_component[entity_index]);
                let u8_component = u8::from_bytes(&u8_component[entity_index]);

                assert_eq!(0xFFBB0065u32, u32_component);
                assert_eq!(0x65u8, u8_component);

                invocation_count += 1;
            });
        });

        registry.query([32].into(), [16].into(), |archetype| {
            let component_indices: Vec<_> =
                archetype.component_heap_indices([32, 8].iter()).collect();

            assert_eq!(2, component_indices.len());
            assert!(component_indices[0].is_some());
            assert!(component_indices[1].is_some());

            let u32_component =
                unsafe { archetype.component_heap_at_index_unsafe(component_indices[0].unwrap()) };
            let u8_component =
                unsafe { archetype.component_heap_at_index_unsafe(component_indices[1].unwrap()) };

            archetype.entity_heap_indices().for_each(|entity_index| {
                assert_eq!(
                    3515135135135135135u128,
                    archetype.entity_id_at_index(entity_index)
                );

                let u32_component = u32::from_bytes(&u32_component[entity_index]);
                let u8_component = u8::from_bytes(&u8_component[entity_index]);

                assert_eq!(0xFFBB0065u32, u32_component);
                assert_eq!(0x65u8, u8_component);

                invocation_count += 1;
            });
        });

        registry.query([32].into(), [8].into(), |_| {
            panic!("Should never be called");
        });
        assert_eq!(2, invocation_count);
    }
}

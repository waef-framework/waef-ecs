use std::{
    collections::{BTreeSet, HashMap},
    hash::Hash,
};

use super::super::archetype_store::ArchetypeStore;

#[derive(Clone, PartialEq, Eq)]
struct ArchetypeKey<ComponentKey : Eq + Hash + Ord>(pub BTreeSet<ComponentKey>);
impl <ComponentKey : Eq + Hash + Ord> Hash for ArchetypeKey<ComponentKey> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.len().hash(state);
        self.0
            .iter()
            .for_each(|component_name| component_name.hash(state));
    }
}
impl <ComponentKey : Eq + Hash + Ord> ArchetypeKey<ComponentKey> {
    pub fn contains(&self, subset: &BTreeSet<ComponentKey>) -> bool {
        self.0.intersection(subset).count() == subset.len()
    }

    pub fn excludes(&self, subset: &BTreeSet<ComponentKey>) -> bool {
        self.0.intersection(subset).count() == 0
    }
}

pub struct ArchetypeRegistry<ComponentKey : Eq + Hash + Ord> {
    archetype_capacity: usize,
    archetypes: HashMap<ArchetypeKey<ComponentKey>, ArchetypeStore<ComponentKey>>,
    entity_archetypes: HashMap<u128, ArchetypeKey<ComponentKey>>,
}
#[allow(unused)]
impl<'a, ComponentKey : Eq + Hash + Clone + Ord> ArchetypeRegistry<ComponentKey> {
    pub fn with_capacity(
        entity_capacity: usize,
        component_capacity: usize,
        archetype_capacity: usize,
    ) -> Self {
        Self {
            archetype_capacity,
            entity_archetypes: HashMap::with_capacity(entity_capacity),
            archetypes: HashMap::with_capacity(component_capacity),
        }
    }

    pub fn apply(&mut self, callback: impl FnOnce(&mut Self)) {
        callback(self)
    }

    pub fn insert_entity(&mut self, entity_id: u128, components: HashMap<ComponentKey, Box<[u8]>>) {
        if components.is_empty() {
            self.entity_archetypes
                .insert(entity_id, ArchetypeKey(BTreeSet::new()));
            return;
        }

        let component_keys: BTreeSet<_> = components.keys().cloned().collect();
        let archetype_key = ArchetypeKey(component_keys);
        if let Some(archetype_store) = self.archetypes.get_mut(&archetype_key) {
            archetype_store.insert(entity_id, components);
            self.entity_archetypes.insert(entity_id, archetype_key);
        } else {
            let component_definition: HashMap<ComponentKey, usize> = components
                .iter()
                .map(|(key, component)| (key.clone(), component.len()))
                .collect();

            let mut archetype_store =
                ArchetypeStore::with_capacity(self.archetype_capacity, component_definition);
            archetype_store.insert(entity_id, components);
            self.archetypes
                .insert(archetype_key.clone(), archetype_store);
            self.entity_archetypes.insert(entity_id, archetype_key);
        }
    }

    pub fn remove_entity(&mut self, entity_id: u128) -> Option<HashMap<ComponentKey, Box<[u8]>>> {
        if let Some(archetype_key) = self.entity_archetypes.remove(&entity_id) {
            if let Some(archetype_store) = self.archetypes.get_mut(&archetype_key) {
                let result = archetype_store.remove(entity_id);
                if archetype_store.is_empty() {
                    self.archetypes.remove(&archetype_key);
                }
                result
            } else {
                Some(HashMap::new())
            }
        } else {
            None
        }
    }

    pub fn query(
        &'a self,
        component_includes: BTreeSet<ComponentKey>,
        component_excludes: BTreeSet<ComponentKey>,
    ) -> impl Iterator<Item = &'a ArchetypeStore<ComponentKey>> + '_ {
        self.archetypes
            .iter()
            .filter_map(move |(archetype_components, store)| {
                if archetype_components.contains(&component_includes)
                    && archetype_components.excludes(&component_excludes)
                {
                    Some(store)
                } else {
                    None
                }
            })
    }
}

#[cfg(test)]
mod tests {
    use core::panic;

    use super::ArchetypeRegistry;
    use waef_core::Pod;

    #[test]
    pub fn archetype_registry_single_test() {
        let mut registry = ArchetypeRegistry::<u32>::with_capacity(1048576, 1024, 32);

        let entity_id = 3515135135135135135u128;
        registry.insert_entity(
            entity_id,
            [
                (
                    32,
                    u32::as_bytes(&0xFFBB0065u32).to_vec().into_boxed_slice(),
                ),
                (
                    8,
                    u8::as_bytes(&0x65u8).to_vec().into_boxed_slice(),
                ),
            ]
            .into(),
        );

        let mut invocation_count = 0;
        for archetype in registry.query([32].into(), [].into()) {
            let component_indices: Vec<_> = archetype
                .component_heap_indices([32, 8].iter())
                .collect();

            assert_eq!(2, component_indices.len());
            assert!(component_indices[0].is_some());
            assert!(component_indices[1].is_some());

            let u32_component =
                unsafe { archetype.component_heap_at_index_unsafe(component_indices[0].unwrap()) };
            let u8_component =
                unsafe { archetype.component_heap_at_index_unsafe(component_indices[1].unwrap()) };

            archetype.entity_heap_indices().for_each(|entity_index| {
                assert_eq!(
                    3515135135135135135u128,
                    archetype.entity_id_at_index(entity_index)
                );

                let u32_component = u32::from_bytes(&u32_component[entity_index]);
                let u8_component = u8::from_bytes(&u8_component[entity_index]);

                assert_eq!(0xFFBB0065u32, u32_component);
                assert_eq!(0x65u8, u8_component);

                invocation_count += 1;
            });
        }

        for archetype in registry.query([32].into(), [16].into()) {
            let component_indices: Vec<_> = archetype
                .component_heap_indices([32, 8].iter())
                .collect();

            assert_eq!(2, component_indices.len());
            assert!(component_indices[0].is_some());
            assert!(component_indices[1].is_some());

            let u32_component =
                unsafe { archetype.component_heap_at_index_unsafe(component_indices[0].unwrap()) };
            let u8_component =
                unsafe { archetype.component_heap_at_index_unsafe(component_indices[1].unwrap()) };

            archetype.entity_heap_indices().for_each(|entity_index| {
                assert_eq!(
                    3515135135135135135u128,
                    archetype.entity_id_at_index(entity_index)
                );

                let u32_component = u32::from_bytes(&u32_component[entity_index]);
                let u8_component = u8::from_bytes(&u8_component[entity_index]);

                assert_eq!(0xFFBB0065u32, u32_component);
                assert_eq!(0x65u8, u8_component);

                invocation_count += 1;
            });
        }

        for _ in registry.query([32].into(), [8].into()) {
            panic!("Should never be called");
        }
        assert_eq!(2, invocation_count);
    }
}

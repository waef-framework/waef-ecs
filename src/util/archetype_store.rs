use std::{cell::UnsafeCell, collections::HashMap, hash::Hash, ops::IndexMut};

use super::deheap::Deheap;

pub struct ArchetypeStore<TComponentKey> {
    component_key_indexes: HashMap<TComponentKey, Option<usize>>,
    entity_ids: Vec<u128>,
    component_layout: UnsafeCell<Vec<Deheap>>,
    entity_count: usize,
}
#[allow(unused)]
impl<'store, ComponentKey: Clone + Eq + Hash> ArchetypeStore<ComponentKey> {
    pub fn with_capacity(
        entity_capacity: usize,
        component_definitions: impl IntoIterator<Item = (ComponentKey, usize)>,
    ) -> Self {
        let component_definitions: Vec<_> = component_definitions.into_iter().collect();

        let component_key_indexes: HashMap<ComponentKey, Option<usize>> = component_definitions
            .iter()
            .enumerate()
            .map(|(index, (key, size))| {
                if *size == 0 {
                    (key.clone(), None)
                } else {
                    (key.clone(), Some(index))
                }
            })
            .collect();

        let component_layout: Vec<_> = component_definitions
            .into_iter()
            .map(|(_, size)| Deheap::with_capacity(size, entity_capacity))
            .collect();

        Self {
            component_key_indexes,
            entity_ids: Vec::with_capacity(entity_capacity),
            component_layout: UnsafeCell::new(component_layout),
            entity_count: 0,
        }
    }

    pub fn insert(
        &mut self,
        entity_id: u128,
        components: impl IntoIterator<Item = (ComponentKey, Box<[u8]>)>,
    ) {
        let _span = tracing::trace_span!("ecs_archetype_store::insert", entity_id).entered();

        if let Some(index) = self
            .entity_ids
            .iter()
            .enumerate()
            .find_map(|(index, item)| if *item == 0 { Some(index) } else { None })
        {
            self.entity_ids[index] = entity_id;
            components.into_iter().for_each(|(component_name, data)| {
                if let Some(component_index) =
                    self.component_key_indexes.get(&component_name).unwrap()
                {
                    self.component_layout.get_mut()[*component_index][index].copy_from_slice(&data);
                }
            });
        } else {
            self.entity_ids.push(entity_id);
            components.into_iter().for_each(|(component_name, data)| {
                if let Some(component_index) =
                    self.component_key_indexes.get(&component_name).unwrap()
                {
                    self.component_layout.get_mut()[*component_index].push(&data);
                }
            });
        }
        self.entity_count += 1;
    }

    pub fn remove(&mut self, entity_id: u128) -> Option<HashMap<ComponentKey, Box<[u8]>>> {
        let _span = tracing::trace_span!("ecs_archetype_store::remove", entity_id).entered();

        if let Some(entity_index) = self
            .entity_ids
            .iter()
            .enumerate()
            .find_map(|(index, item)| {
                if *item == entity_id {
                    Some(index)
                } else {
                    None
                }
            })
        {
            self.entity_ids[entity_index] = 0;
            let result = self
                .component_key_indexes
                .iter()
                .map(|(key, component_index)| {
                    if let Some(component_index) = component_index {
                        let component_heap = &mut self.component_layout.get_mut()[*component_index];
                        let mut target = vec![0; component_heap.item_len()];
                        component_heap[entity_index].clone_into(&mut target);
                        (key.clone(), target.into_boxed_slice())
                    } else {
                        (key.clone(), vec![].into_boxed_slice())
                    }
                })
                .collect();

            self.entity_count -= 1;
            Some(result)
        } else {
            None
        }
    }

    pub fn component_heap_indices(
        &'store self,
        components: impl Iterator<Item = &'store ComponentKey> + 'store,
    ) -> impl Iterator<Item = Option<usize>> + 'store {
        components.map(|component| self.component_key_indexes.get(component).cloned().flatten())
    }

    pub fn entity_heap_index_for_entity_id(&self, entity_id: u128) -> Option<usize> {
        self.entity_ids.iter().enumerate().find_map(|(index, e)| {
            if *e == entity_id {
                Some(index)
            } else {
                None
            }
        })
    }

    pub fn entity_heap_indices(&self) -> impl Iterator<Item = usize> + '_ {
        self.entity_ids
            .iter()
            .enumerate()
            .filter_map(|(index, e)| if *e != 0 { Some(index) } else { None })
    }

    pub fn component_heap_at_index(
        &self,
        component_index: usize,
    ) -> &impl IndexMut<usize, Output = [u8]> {
        unsafe { &(*self.component_layout.get())[component_index] }
    }

    pub fn component_heap_at_index_mut(
        &mut self,
        component_index: usize,
    ) -> &mut impl IndexMut<usize, Output = [u8]> {
        &mut (self.component_layout.get_mut())[component_index]
    }

    pub unsafe fn component_heap_at_index_unsafe(
        &self,
        component_index: usize,
    ) -> &mut impl IndexMut<usize, Output = [u8]> {
        &mut (*self.component_layout.get())[component_index]
    }

    pub fn entity_id_at_index(&self, entity_index: usize) -> u128 {
        self.entity_ids[entity_index]
    }

    pub fn len(&self) -> usize {
        self.entity_count
    }

    pub fn is_empty(&self) -> bool {
        self.entity_count == 0
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use waef_core::Pod;

    use super::ArchetypeStore;

    fn generate_component(index: usize) -> (u128, HashMap<u32, Box<[u8]>>) {
        let entity_id = 864513845468554846854786u128 + index as u128;

        let mut components: HashMap<u32, Box<[u8]>> = HashMap::new();
        components.insert(
            32,
            Pod::as_vec(&(0xFFBB0065u32 + index as u32)).into_boxed_slice(),
        );
        components.insert(8, Pod::as_vec(&(0x22u8 + index as u8)).into_boxed_slice());
        components.insert(0, vec![].into_boxed_slice());

        (entity_id, components)
    }

    #[test]
    pub fn indices_test() {
        let store = ArchetypeStore::<u32>::with_capacity(1024, [(32, 4), (8, 1), (0, 0)]);

        let result: Vec<_> = store
            .component_heap_indices([32, 0, 6942069].iter())
            .collect();
        assert_eq!(result.len(), 3);
        assert!(result[0].is_some());
        assert!(result[1].is_none()); // tags and missing both lack an index
        assert!(result[2].is_none());
    }

    #[test]
    pub fn single_entity_component_test() {
        let mut store = ArchetypeStore::with_capacity(1024, [(32, 4), (8, 1), (0, 0)]);

        let (entity_id, components) = generate_component(0);
        store.insert(entity_id, components.clone());

        let component_indices: Vec<_> = store.component_heap_indices([32, 8, 0].iter()).collect();

        let entity_indices: Vec<_> = store.entity_heap_indices().collect();
        assert_eq!(1, entity_indices.len());

        let mut iteration_count = 0;
        for index in entity_indices {
            iteration_count += 1;

            let (expected_entity_id, expected_components) = generate_component(index);

            let actual_entity_id = store.entity_id_at_index(index);
            assert_eq!(expected_entity_id, actual_entity_id);

            let expected_u32 = u32::from_bytes(&expected_components[&32]);
            let expected_u8 = u8::from_bytes(&expected_components[&8]);

            let actual_u32 = u32::from_bytes(
                &store.component_heap_at_index(component_indices[0].unwrap())[index],
            );
            let actual_u8 = u8::from_bytes(
                &store.component_heap_at_index(component_indices[1].unwrap())[index],
            );

            assert_eq!(expected_u32, actual_u32);
            assert_eq!(expected_u8, actual_u8);
        }
        assert_eq!(1, iteration_count);
    }

    #[test]
    pub fn single_entity_component_mutation_test() {
        let mut store = ArchetypeStore::with_capacity(1024, [(32, 4), (8, 1), (0, 0)]);

        let (entity_id, components) = generate_component(0);
        store.insert(entity_id, components.clone());

        let component_indices: Vec<_> = store.component_heap_indices([32, 8, 0].iter()).collect();

        let entity_indices: Vec<_> = store.entity_heap_indices().collect();
        assert_eq!(1, entity_indices.len());

        let mut iteration_count = 0;
        for index in entity_indices {
            iteration_count += 1;

            let (expected_entity_id, expected_components) = generate_component(index);

            let actual_entity_id = store.entity_id_at_index(index);
            assert_eq!(expected_entity_id, actual_entity_id);

            let expected_u32 = u32::from_bytes(&expected_components[&32]);
            let expected_u8 = u8::from_bytes(&expected_components[&8]);

            let actual_u32_bytes_ptr: *mut u8 = store
                .component_heap_at_index_mut(component_indices[0].unwrap())[index]
                .as_mut_ptr();
            let actual_u8_bytes_ptr: *mut u8 = store
                .component_heap_at_index_mut(component_indices[1].unwrap())[index]
                .as_mut_ptr();

            let actual_u32_bytes =
                unsafe { std::slice::from_raw_parts_mut(actual_u32_bytes_ptr, 4) };
            let actual_u8_bytes = unsafe { std::slice::from_raw_parts_mut(actual_u8_bytes_ptr, 1) };

            actual_u32_bytes.copy_from_slice(u32::as_bytes(&(&expected_u32 + 10)));
            actual_u8_bytes.copy_from_slice(u8::as_bytes(&(&expected_u8 + 10)));

            let actual_u32 = u32::from_bytes(
                &store.component_heap_at_index(component_indices[0].unwrap())[index],
            );
            let actual_u8 = u8::from_bytes(
                &store.component_heap_at_index(component_indices[1].unwrap())[index],
            );

            assert_eq!(expected_u32 + 10, actual_u32);
            assert_eq!(expected_u8 + 10, actual_u8);
        }
        assert_eq!(1, iteration_count);
    }

    #[test]
    pub fn single_remove_entity_component_test() {
        let mut store = ArchetypeStore::with_capacity(1024, [(32, 4), (8, 1), (0, 0)]);

        let (entity_id, components) = generate_component(0);
        store.insert(entity_id, components.clone());

        let removed = store.remove(entity_id);
        assert!(removed.is_some());

        let entity_indices: Vec<_> = store.entity_heap_indices().collect();
        assert_eq!(0, entity_indices.len());

        let removed = removed.unwrap();

        let (_, expected_components) = generate_component(0);

        let expected_u32 = u32::from_bytes(&expected_components[&32]);
        let expected_u8 = u8::from_bytes(&expected_components[&8]);

        let actual_u32 = u32::from_bytes(&removed[&32]);
        let actual_u8 = u8::from_bytes(&removed[&8]);

        assert_eq!(expected_u32, actual_u32);
        assert_eq!(expected_u8, actual_u8);
    }
}

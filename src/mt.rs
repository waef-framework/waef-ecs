use parking_lot::RwLock;
use std::{
    collections::{BTreeSet, HashMap, HashSet},
    fmt::Debug,
    hash::Hash,
    ptr::null_mut,
    sync::{mpsc::Sender, Arc},
};
use waef_core::{
    ecs::messages::{
        OnComponentDefined, OnEntityComponentAttached, OnEntityComponentDetached, OnEntityCreated,
        OnEntityDestroyed,
    },
    EcsRegistry, IsMessage, Message, WaefError,
};

use crate::util::{archetype_store::ArchetypeStore, registry::ArchetypeRegistry};

#[cfg(not(feature = "component_key_as_string"))]
use std::sync::atomic::{AtomicU32, Ordering};

#[cfg(not(feature = "component_key_as_string"))]
#[derive(Hash, PartialEq, Eq, PartialOrd, Ord, Clone)]
struct ComponentKey(u32);

#[cfg(not(feature = "component_key_as_string"))]
impl ComponentKey {
    pub fn new(val: u32) -> Self {
        Self(val)
    }
    pub fn zero() -> Self {
        Self(0)
    }
}

#[cfg(feature = "component_key_as_string")]
#[derive(Hash, PartialEq, Eq, PartialOrd, Ord, Clone)]
struct ComponentKey(String);

#[cfg(feature = "component_key_as_string")]
impl ComponentKey {
    pub fn new(val: String) -> Self {
        Self(val)
    }
    pub fn zero() -> Self {
        Self("".into())
    }
}

impl Debug for ComponentKey {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Default)]
struct ComponentQuery {
    exclude: BTreeSet<ComponentKey>,
    include: BTreeSet<ComponentKey>,
    optional: BTreeSet<ComponentKey>,
}

#[derive(Default)]
struct ComponentQueryDefinitions {
    component_ids: Vec<ComponentKey>,
    component_sizes: Vec<usize>,
}

pub struct WaefEcsRegistry {
    #[cfg(not(feature = "component_key_as_string"))]
    next_component_id: AtomicU32,

    component_definitions: RwLock<HashMap<String, (usize, ComponentKey)>>,
    component_keys: RwLock<HashMap<ComponentKey, String>>,
    archetype_registry: ArchetypeRegistry<ComponentKey>,

    sender: Sender<Message>,
}
impl WaefEcsRegistry {
    pub fn new(dispatcher: Sender<Message>) -> Self {
        Self {
            #[cfg(not(feature = "component_key_as_string"))]
            next_component_id: AtomicU32::new(1),

            component_definitions: RwLock::new(HashMap::with_capacity(32)),
            component_keys: RwLock::new(HashMap::with_capacity(32)),
            archetype_registry: ArchetypeRegistry::with_capacity(1024, 32, 64),
            sender: dispatcher,
        }
    }

    pub fn new_arc(dispatcher: Sender<Message>) -> Arc<dyn EcsRegistry> {
        Arc::new(Self::new(dispatcher))
    }

    #[cfg(not(feature = "component_key_as_string"))]
    fn generate_component_id(&self, _component_name: &String) -> ComponentKey {
        ComponentKey::new(self.next_component_id.fetch_add(1, Ordering::SeqCst))
    }

    #[cfg(feature = "component_key_as_string")]
    fn generate_component_id(&self, _component_name: &String) -> ComponentKey {
        ComponentKey::new(_component_name.clone())
    }

    fn get_or_create_component_id(&self, component_name: &String, size: usize) -> ComponentKey {
        let mut component_definitions = self.component_definitions.write();

        if let Some(id) = component_definitions
            .get(component_name)
            .cloned()
            .map(|(_, id)| id)
        {
            id
        } else {
            let id = self.generate_component_id(&component_name);
            component_definitions.insert(component_name.clone(), (size, id.clone()));
            self.component_keys
                .write()
                .insert(id.clone(), component_name.clone());
            id
        }
    }

    fn get_component_id(&self, component_name: &String) -> Option<ComponentKey> {
        let component_definitions = self.component_definitions.read();

        component_definitions
            .get(component_name)
            .cloned()
            .map(|(_, id)| id)
    }

    fn parse_query(
        &self,
        component_query: &[String],
    ) -> (ComponentQuery, ComponentQueryDefinitions) {
        let _span = tracing::trace_span!(
            "ecs_registry::parse_query",
            query = component_query.join(" ")
        )
        .entered();

        let component_definitions = self.component_definitions.read();

        let mut query = ComponentQuery::default();
        let mut query_definitions = ComponentQueryDefinitions::default();
        component_query.into_iter().for_each(|component_name| {
            match component_name.chars().nth(0) {
                Some('!') => {
                    let component_name = component_name[1..].to_string();
                    let def = component_definitions
                        .get(&component_name)
                        .cloned()
                        .unwrap_or((0, ComponentKey::zero()));

                    query.exclude.insert(def.1);
                }
                Some('?') => {
                    let component_name = component_name[1..].to_string();
                    let def = component_definitions
                        .get(&component_name)
                        .cloned()
                        .unwrap_or((0, ComponentKey::zero()));

                    query.optional.insert(def.1.clone());
                    query_definitions.component_sizes.push(def.0);
                    query_definitions.component_ids.push(def.1);
                }
                Some(_) => {
                    let def = component_definitions
                        .get(component_name)
                        .cloned()
                        .unwrap_or((0, ComponentKey::zero()));

                    query.include.insert(def.1.clone());
                    query_definitions.component_sizes.push(def.0);
                    query_definitions.component_ids.push(def.1);
                }
                None => {}
            }
        });
        (query, query_definitions)
    }

    fn apply_archetype_store(
        &self,
        component_query: &[String],
        callback: &mut dyn FnMut(&ComponentQueryDefinitions, &ArchetypeStore<ComponentKey>),
    ) {
        let _span = tracing::trace_span!(
            "ecs_registry::apply_archetype_store",
            query = component_query.join(" ")
        )
        .entered();

        let (component_query, component_query_definitions) = self.parse_query(component_query);

        self.archetype_registry
            .query(component_query.include, component_query.exclude, |store| {
                let _span = tracing::trace_span!("ecs_registry::apply_archetype_store::archetype")
                    .entered();

                callback(&component_query_definitions, store)
            });
    }
}
unsafe impl Sync for WaefEcsRegistry {}
unsafe impl Send for WaefEcsRegistry {}

impl EcsRegistry for WaefEcsRegistry {
    fn list_component_definitions(&self) -> HashMap<String, usize> {
        let _guard = tracing::trace_span!("ecs::list_component_definitions").entered();

        let component_definitions = self.component_definitions.read();

        component_definitions
            .iter()
            .map(|(key, (size, _))| (key.clone(), *size))
            .collect()
    }

    fn define_component_by_size(&self, name: String, size: usize) -> Result<(), WaefError> {
        let _guard = tracing::trace_span!(
            "ecs::define_component_by_size",
            component_name = name,
            component_size = size
        )
        .entered();

        let mut component_definitions = self.component_definitions.write();

        if !component_definitions.contains_key(&name) {
            let component_id = self.generate_component_id(&name);
            component_definitions.insert(name.clone(), (size, component_id.clone()));
            self.component_keys
                .write()
                .insert(component_id, name.clone());

            _ = self.sender.send(
                OnComponentDefined {
                    component_name: name,
                    component_size: size as u32,
                }
                .into_message(),
            );
        }
        Ok(())
    }

    fn get_component_size(&self, name: String) -> Option<usize> {
        let _guard =
            tracing::trace_span!("ecs::get_component_size", component_name = name).entered();

        let component_definitions = self.component_definitions.read();

        component_definitions
            .get(&name)
            .cloned()
            .map(|(size, _)| size)
    }

    fn create_entity(&self, entity_id: u128) -> Result<(), WaefError> {
        let _guard = tracing::trace_span!("ecs::create_entity", entity_id).entered();

        self.archetype_registry
            .insert_entity(entity_id, HashMap::new());

        self.sender
            .send(OnEntityCreated { entity_id }.into_message())
            .map_err(WaefError::new)?;
        Ok(())
    }

    fn destroy_entity(&self, entity_id: u128) -> Result<(), WaefError> {
        let _guard = tracing::trace_span!("ecs::destroy_entity", entity_id).entered();

        if let Some(entity_components) = self.archetype_registry.remove_entity(entity_id) {
            let component_keys = self.component_keys.read_recursive();
            for component_name in entity_components
                .keys()
                .flat_map(|key| component_keys.get(key))
                .cloned()
            {
                self.sender
                    .send(
                        OnEntityComponentDetached {
                            entity_id,
                            component_name,
                        }
                        .into_message(),
                    )
                    .map_err(WaefError::new)?;
            }
            self.sender
                .send(OnEntityDestroyed { entity_id }.into_message())
                .map_err(WaefError::new)?;
            Ok(())
        } else {
            Err(WaefError::new("entity not found"))
        }
    }

    fn attach_component_buffer(
        &self,
        component_name: String,
        entity_id: u128,
        component_buffer: Box<[u8]>,
    ) -> Result<(), WaefError> {
        let _guard =
            tracing::trace_span!("ecs::attach_component_buffer", entity_id, component_name)
                .entered();

        let component_id = self.get_or_create_component_id(&component_name, component_buffer.len());

        self.archetype_registry
            .add_component_to_entity(entity_id, (component_id, component_buffer))?;

        self.sender
            .send(
                OnEntityComponentAttached {
                    entity_id,
                    component_name,
                }
                .into_message(),
            )
            .map_err(WaefError::new)?;

        Ok(())
    }

    fn detach_component_by_name(
        &self,
        component_name: String,
        entity_id: u128,
    ) -> Result<(), WaefError> {
        let _guard =
            tracing::trace_span!("ecs::detach_component_by_name", entity_id, component_name)
                .entered();

        if let Some(component_id) = self.get_component_id(&component_name) {
            self.archetype_registry
                .remove_component_from_entity(entity_id, component_id)?;

            self.sender
                .send(
                    OnEntityComponentDetached {
                        entity_id,
                        component_name,
                    }
                    .into_message(),
                )
                .map_err(WaefError::new)?;
            Ok(())
        } else {
            Err(WaefError::new("component not found"))
        }
    }

    fn create_entity_with_components(
        &self,
        entity_id: u128,
        components: HashMap<String, Box<[u8]>>,
    ) -> Result<(), WaefError> {
        let _guard = tracing::trace_span!(
            "ecs::create_entity_with_components",
            entity_id = entity_id,
            components = components.keys().cloned().collect::<Vec<_>>().join(" ")
        )
        .entered();

        if components.is_empty() {
            self.archetype_registry
                .insert_entity(entity_id, HashMap::new());

            _ = self
                .sender
                .send(OnEntityCreated { entity_id }.into_message());
            Ok(())
        } else {
            let component_names: Vec<_> = components.keys().cloned().collect();
            let component_map = components
                .into_iter()
                .map(|(component_name, component_buffer)| {
                    let component_key =
                        self.get_or_create_component_id(&component_name, component_buffer.len());
                    (component_key, component_buffer)
                })
                .collect();

            self.archetype_registry
                .insert_entity(entity_id, component_map);

            _ = self
                .sender
                .send(OnEntityCreated { entity_id }.into_message());

            for component_name in component_names {
                self.sender
                    .send(
                        OnEntityComponentAttached {
                            entity_id,
                            component_name,
                        }
                        .into_message(),
                    )
                    .map_err(WaefError::new)?;
            }
            Ok(())
        }
    }

    fn apply(&self, component_query: &[String], callback: &mut dyn FnMut(u128, &mut [&mut [u8]])) {
        let _guard =
            tracing::trace_span!("ecs::apply", component_query = component_query.join(" "))
                .entered();

        let mut data_store = Vec::with_capacity(component_query.len());
        self.apply_unsafe(component_query, &mut |entity_id, entity_ptrs| {
            let max_buffer_size = entity_ptrs.iter().map(|x| x.1).max().unwrap_or(32usize);
            let mut empty_buffer = vec![0u8; max_buffer_size];

            data_store.clear();
            data_store.extend(entity_ptrs.iter().map(|items| {
                let ptr = items.0;
                let size = items.1;
                if ptr.is_null() {
                    unsafe { std::slice::from_raw_parts_mut(empty_buffer.as_mut_ptr(), size) }
                } else {
                    unsafe { std::slice::from_raw_parts_mut(ptr, size) }
                }
            }));
            callback(entity_id, &mut data_store[..]);
        });
    }

    fn apply_set(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_set",
            entity_count = entity_ids.len(),
            component_query = component_query.join(" ")
        )
        .entered();

        let mut data_store = Vec::with_capacity(component_query.len());
        self.apply_set_unsafe(
            entity_ids,
            component_query,
            &mut |entity_id, entity_ptrs| {
                let max_buffer_size = entity_ptrs.iter().map(|x| x.1).max().unwrap_or(32usize);
                let mut empty_buffer = vec![0u8; max_buffer_size];

                data_store.clear();
                data_store.extend(entity_ptrs.iter().map(|items| {
                    let ptr = items.0;
                    let size = items.1;
                    if ptr.is_null() {
                        unsafe { std::slice::from_raw_parts_mut(empty_buffer.as_mut_ptr(), size) }
                    } else {
                        unsafe { std::slice::from_raw_parts_mut(ptr, size) }
                    }
                }));
                callback(entity_id, &mut data_store[..]);
            },
        );
    }

    fn apply_single(
        &self,
        entity_id: u128,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_single",
            entity_id = entity_id,
            component_query = component_query.join(" ")
        )
        .entered();

        let mut data_store = Vec::with_capacity(component_query.len());
        self.apply_single_unsafe(entity_id, component_query, &mut |entity_id, entity_ptrs| {
            let max_buffer_size = entity_ptrs.iter().map(|x| x.1).max().unwrap_or(32usize);
            let mut empty_buffer = vec![0u8; max_buffer_size];

            data_store.clear();
            data_store.extend(entity_ptrs.iter().map(|items| {
                let ptr = items.0;
                let size = items.1;
                if ptr.is_null() {
                    unsafe { std::slice::from_raw_parts_mut(empty_buffer.as_mut_ptr(), size) }
                } else {
                    unsafe { std::slice::from_raw_parts_mut(ptr, size) }
                }
            }));
            callback(entity_id, &mut data_store[..]);
        });
    }

    fn apply_set_unsafe(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_set_unsafe",
            entity_count = entity_ids.len(),
            component_query = component_query.join(" ")
        )
        .entered();

        self.apply_set_unsafe_batched(entity_ids, component_query, &mut |items| {
            items
                .iter()
                .for_each(|(entity_id, items)| callback(*entity_id, *items))
        });
    }

    fn apply_unsafe(
        &self,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_unsafe",
            component_query = component_query.join(" ")
        )
        .entered();

        self.apply_unsafe_batched(component_query, &mut |items| {
            items
                .iter()
                .for_each(|(entity_id, items)| callback(*entity_id, *items))
        });
    }

    fn apply_single_unsafe(
        &self,
        entity_id: u128,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_single_unsafe",
            entity_id = entity_id,
            component_query = component_query.join(" ")
        )
        .entered();

        self.apply_archetype_store(
            component_query,
            &mut |component_query_definitions: &ComponentQueryDefinitions,
                  store: &ArchetypeStore<ComponentKey>| unsafe {
                let mut component_heaps: Vec<_> = store
                    .component_heap_indices(component_query_definitions.component_ids.iter())
                    .map(|index| match index {
                        Some(index) => Some(store.component_heap_at_index_unsafe(index)),
                        None => None,
                    })
                    .enumerate()
                    .collect();

                let mut callback_data: Vec<(*mut u8, usize)> =
                    vec![(null_mut(), 0); component_query_definitions.component_ids.len()];
                if let Some(heap_index) = store.entity_heap_index_for_entity_id(entity_id) {
                    component_heaps
                        .iter_mut()
                        .for_each(|(callback_index, heap)| match heap {
                            Some(heap) => {
                                callback_data[*callback_index] = (
                                    heap[heap_index].as_mut_ptr(),
                                    component_query_definitions.component_sizes[*callback_index],
                                );
                            }
                            None => {
                                callback_data[*callback_index] = (
                                    null_mut(),
                                    component_query_definitions.component_sizes[*callback_index],
                                );
                            }
                        });
                    callback(entity_id, &callback_data);
                }
            },
        )
    }

    fn apply_unsafe_batched(
        &self,
        component_query: &[String],
        callback: &mut dyn FnMut(&[(u128, &[(*mut u8, usize)])]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_unsafe_batched",
            component_query = component_query.join(" ")
        )
        .entered();

        self.apply_archetype_store(
            component_query,
            &mut |component_query_definitions: &ComponentQueryDefinitions,
                  store: &ArchetypeStore<ComponentKey>| unsafe {
                let mut component_heaps: Vec<_> = store
                    .component_heap_indices(component_query_definitions.component_ids.iter())
                    .map(|index| match index {
                        Some(index) => Some(store.component_heap_at_index_unsafe(index)),
                        None => None,
                    })
                    .enumerate()
                    .collect();

                let mut callback_data: Vec<(*mut u8, usize)> =
                    Vec::with_capacity(store.len() * component_heaps.len());
                let mut entity_callback_data: Vec<(u128, &[(*mut u8, usize)])> =
                    Vec::with_capacity(store.len());

                store.entity_heap_indices().for_each(&mut |heap_index| {
                    let entity_id = store.entity_id_at_index(heap_index);
                    component_heaps
                        .iter_mut()
                        .for_each(|(callback_index, heap)| match heap {
                            Some(heap) => {
                                callback_data.push((
                                    heap[heap_index].as_mut_ptr(),
                                    component_query_definitions.component_sizes[*callback_index],
                                ));
                            }
                            None => {
                                callback_data.push((
                                    null_mut(),
                                    component_query_definitions.component_sizes[*callback_index],
                                ));
                            }
                        });
                    entity_callback_data.push((entity_id, &[]));
                });
                entity_callback_data
                    .iter_mut()
                    .enumerate()
                    .for_each(|(index, result)| {
                        let head = index * component_heaps.len();
                        let tail = head + component_heaps.len();
                        result.1 = &callback_data[head..tail];
                    });
                callback(&entity_callback_data);
            },
        )
    }

    fn apply_set_unsafe_batched(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(&[(u128, &[(*mut u8, usize)])]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_set_unsafe_batched",
            entity_count = entity_ids.len(),
            component_query = component_query.join(" ")
        )
        .entered();

        let entity_id_set: HashSet<u128> = HashSet::from_iter(entity_ids.iter().cloned());
        self.apply_archetype_store(
            component_query,
            &mut |component_query_definitions: &ComponentQueryDefinitions,
                  store: &ArchetypeStore<ComponentKey>| unsafe {
                let mut component_heaps: Vec<_> = store
                    .component_heap_indices(component_query_definitions.component_ids.iter())
                    .map(|index| match index {
                        Some(index) => Some(store.component_heap_at_index_unsafe(index)),
                        None => None,
                    })
                    .enumerate()
                    .collect();

                let mut callback_data: Vec<(*mut u8, usize)> =
                    Vec::with_capacity(entity_ids.len() * component_heaps.len());
                let mut entity_callback_data: Vec<(u128, &[(*mut u8, usize)])> =
                    Vec::with_capacity(entity_ids.len());

                store.entity_heap_indices().for_each(&mut |heap_index| {
                    let entity_id = store.entity_id_at_index(heap_index);
                    if !entity_id_set.contains(&entity_id) {
                        return;
                    }

                    component_heaps
                        .iter_mut()
                        .for_each(|(callback_index, heap)| match heap {
                            Some(heap) => {
                                callback_data.push((
                                    heap[heap_index].as_mut_ptr(),
                                    component_query_definitions.component_sizes[*callback_index],
                                ));
                            }
                            None => {
                                callback_data.push((
                                    null_mut(),
                                    component_query_definitions.component_sizes[*callback_index],
                                ));
                            }
                        });
                    entity_callback_data.push((entity_id, &[]));
                });
                entity_callback_data
                    .iter_mut()
                    .enumerate()
                    .for_each(|(index, result)| {
                        let head = index * component_heaps.len();
                        let tail = head + component_heaps.len();
                        result.1 = &callback_data[head..tail];
                    });
                callback(&entity_callback_data);
            },
        )
    }
}

#[cfg(test)]
mod tests {
    use std::hash::{DefaultHasher, Hasher};
    use std::num::Wrapping;
    use std::sync::atomic::{AtomicU64, Ordering};
    use std::sync::mpsc::channel;
    use std::sync::{Arc, Barrier};
    use std::thread::{self};

    use super::WaefEcsRegistry;
    use waef_core::core::WaefError;
    use waef_core::util::pod::Pod;
    use waef_core::{component, EcsExtension};

    #[test]
    pub fn removal_whilst_iterating_test() -> std::thread::Result<()> {
        let (sender, _receiver) = channel();
        let registry = WaefEcsRegistry::new_arc(sender);

        registry.define_component_by_size("i32".into(), 4).unwrap();
        registry.define_component_by_size("i64".into(), 8).unwrap();
        registry.define_component_by_size("i16".into(), 2).unwrap();
        for i in 1..10000 {
            registry
                .create_entity_with_components(
                    i,
                    [(
                        "i32".to_string(),
                        (i as u32).as_bytes().to_vec().into_boxed_slice(),
                    )]
                    .into(),
                )
                .unwrap();
        }
        for i in 1..5000 {
            if i % 2 == 0 {
                registry
                    .attach_component_buffer(
                        "i16".into(),
                        i,
                        (i as u16).as_bytes().to_vec().into_boxed_slice(),
                    )
                    .unwrap();
            }
            registry
                .attach_component_buffer(
                    "i64".into(),
                    i,
                    (i as u64).as_bytes().to_vec().into_boxed_slice(),
                )
                .unwrap();
            if i % 2 == 1 {
                registry
                    .attach_component_buffer(
                        "i16".into(),
                        i,
                        (i as u16).as_bytes().to_vec().into_boxed_slice(),
                    )
                    .unwrap();
            }
        }

        let barrier = Arc::new(Barrier::new(3));

        let reg = registry.clone();
        let bar = barrier.clone();
        let apply = thread::spawn(move || {
            bar.wait();
            for _ in 1..10000 {
                reg.apply(&["i32".to_string()], &mut |_, _| {
                    std::thread::yield_now();
                });
            }
        });

        let bar = barrier.clone();
        let reg = registry.clone();
        let remove = thread::spawn(move || {
            bar.wait();
            for i in 1..10000 {
                reg.destroy_entity(i).unwrap();
                std::thread::yield_now();
            }
        });
        barrier.wait();
        remove.join()?;
        apply.join()?;

        registry.apply(&["i32".to_string()], &mut |_, _| {
            assert!(false, "All components should have been removed")
        });
        Ok(())
    }

    #[test]
    pub fn test_ecs() -> Result<(), WaefError> {
        let (sender, _receiver) = channel();
        let ecs = WaefEcsRegistry::new_arc(sender);

        ecs.define_component_by_size("test_component_1".into(), 4)?;
        ecs.define_component_by_size("test_component_2".into(), 8)?;
        ecs.define_component_by_size("test_tag_1".into(), 0)?;

        assert_eq!(ecs.get_component_size("test_component_1".into()), Some(4));
        assert_eq!(ecs.get_component_size("test_component_2".into()), Some(8));
        assert_eq!(ecs.get_component_size("test_component_noop".into()), None);

        let eid_1: u128 = 1;
        ecs.create_entity(eid_1)?;
        ecs.attach_component_buffer("test_component_1".into(), eid_1, 10_u32.into_boxed_slice())?;
        ecs.attach_component_buffer("test_component_2".into(), eid_1, 25_u64.into_boxed_slice())?;
        ecs.attach_component_buffer("test_tag_1".into(), eid_1, Box::new([]))?;
        ecs.detach_component_by_name("test_component_2".into(), eid_1)?;

        let eid_2: u128 = 2;
        ecs.create_entity(eid_2)?;
        ecs.attach_component_buffer("test_component_2".into(), eid_2, 15_u64.into_boxed_slice())?;
        ecs.attach_component_buffer("test_tag_1".into(), eid_2, Box::new([]))?;

        let eid_3: u128 = 3;
        ecs.create_entity(eid_3)?;
        ecs.attach_component_buffer("test_component_1".into(), eid_3, 20_u32.into_boxed_slice())?;
        ecs.attach_component_buffer("test_component_2".into(), eid_3, 30_u64.into_boxed_slice())?;

        let eid_4: u128 = 4;
        ecs.create_entity(eid_4)?;
        ecs.attach_component_buffer("test_component_1".into(), eid_4, 123_u32.into_boxed_slice())?;
        ecs.attach_component_buffer("test_component_2".into(), eid_4, 456_u64.into_boxed_slice())?;
        ecs.attach_component_buffer("test_tag_1".into(), eid_4, Box::new([]))?;
        ecs.destroy_entity(eid_4)?;

        let mut called = 0;
        ecs.apply(&["test_component_1".into()], &mut |id, data| {
            called = called + 1;

            assert_eq!(data.len(), 1);
            assert_eq!(data[0].len(), 4);

            match id {
                _ if id == eid_1 => {
                    assert_eq!(u32::from_bytes(data[0]), 10);
                    data[0].copy_from_slice(11_u32.try_split());
                }
                _ if id == eid_3 => assert_eq!(u32::from_bytes(data[0]), 20),
                _ => panic!("Expected entities {} or {} but was {}", eid_1, eid_3, id),
            }
        });
        assert_eq!(called, 2);

        let mut called = 0;
        ecs.apply_set(
            &[eid_1, eid_3, eid_4],
            &["test_component_1".into()],
            &mut |id, data| {
                called = called + 1;

                assert_eq!(data.len(), 1);
                assert_eq!(data[0].len(), 4);

                match id {
                    _ if id == eid_1 => {
                        assert_eq!(u32::from_bytes(data[0]), 11);
                        data[0].copy_from_slice(11_u32.try_split());
                    }
                    _ if id == eid_3 => assert_eq!(u32::from_bytes(data[0]), 20),
                    _ => panic!("Expected entities {} or {} but was {}", eid_1, eid_3, id),
                }
            },
        );
        assert_eq!(called, 2);

        let mut called = 0;
        ecs.apply(&["test_component_1".into()], &mut |id, data| {
            called = called + 1;
            assert_eq!(data.len(), 1);
            assert_eq!(data[0].len(), 4);

            match id {
                _ if id == eid_1 => assert_eq!(u32::from_bytes(data[0]), 11),
                _ if id == eid_3 => assert_eq!(u32::from_bytes(data[0]), 20),
                _ => panic!("Expected entities {} or {} but was {}", eid_1, eid_3, id),
            }
        });
        assert_eq!(called, 2);

        let mut called = 0;
        ecs.apply(&["test_tag_1".into()], &mut |id, data| {
            called = called + 1;
            assert_eq!(data.len(), 1);
            assert_eq!(data[0].len(), 0);

            match id {
                _ if id == eid_1 => (),
                _ if id == eid_2 => (),
                _ => panic!("Expected entities {} or {} but was {}", eid_1, eid_2, id),
            }
        });
        assert_eq!(called, 2);

        let mut called = 0;
        ecs.apply_single(eid_1, &["test_tag_1".into()], &mut |id, data| {
            called = called + 1;
            assert_eq!(data.len(), 1);
            assert_eq!(data[0].len(), 0);

            match id {
                _ if id == eid_1 => (),
                _ => panic!("Expected entity {} but was {}", eid_1, id),
            }
        });
        assert_eq!(called, 1);

        let mut called = 0;
        ecs.apply(
            &["test_component_1".into(), "test_component_2".into()],
            &mut |id, data| {
                called = called + 1;
                assert_eq!(data.len(), 2);
                assert_eq!(data[0].len(), 4);
                assert_eq!(data[1].len(), 8);

                match id {
                    _ if id == eid_3 => {
                        assert_eq!(u32::from_bytes(data[0]), 20);
                        assert_eq!(u64::from_bytes(data[1]), 30);
                    }
                    _ => panic!("Expected entity {} but was {}", eid_3, id),
                }
            },
        );
        assert_eq!(called, 1);

        let mut called = 0;
        ecs.apply(
            &["test_component_1".into(), "!test_component_2".into()],
            &mut |id, data| {
                called = called + 1;
                assert_eq!(data.len(), 1);

                match id {
                    _ if id == eid_1 => {
                        assert_eq!(u32::from_bytes(data[0]), 11);
                        data[0].copy_from_slice(12_u32.try_split());
                    }
                    _ => panic!("Expected entities {} or {} but was {}", eid_1, eid_3, id),
                }
            },
        );
        assert_eq!(called, 1);

        let mut called = 0;
        ecs.apply(
            &[
                "test_component_1".into(),
                "test_component_nonexistant".into(),
            ],
            &mut |id, _| {
                called = called + 1;
                panic!("Expected to never be called but was called with {}", id);
            },
        );
        assert_eq!(called, 0);

        let mut called = 0;
        ecs.apply(
            &["test_component_1".into(), "test_component_2".into()],
            &mut |_id, _data| {
                called = called + 1;
                _ = ecs.apply(
                    &["test_component_1".into(), "test_component_2".into()],
                    &mut |id, data| {
                        called = called + 1;
                        assert_eq!(data.len(), 2);
                        assert_eq!(data[0].len(), 4);
                        assert_eq!(data[1].len(), 8);

                        match id {
                            _ if id == eid_3 => {
                                assert_eq!(u32::from_bytes(data[0]), 20);
                                assert_eq!(u64::from_bytes(data[1]), 30);
                            }
                            _ => panic!("Expected entity {} but was {}", eid_3, id),
                        }
                    },
                );
            },
        );
        assert_eq!(called, 2);

        let mut called = 0;
        ecs.apply(
            &["test_component_1".into(), "?test_component_2".into()],
            &mut |id, data| {
                called = called + 1;
                assert_eq!(data.len(), 2);
                assert_eq!(data[0].len(), 4);
                assert_eq!(data[1].len(), 8);

                match id {
                    _ if id == eid_1 => {
                        assert_eq!(u32::from_bytes(data[0]), 12);
                        assert_eq!(u64::from_bytes(data[1]), 0);
                    }
                    _ if id == eid_3 => {
                        assert_eq!(u32::from_bytes(data[0]), 20);
                        assert_eq!(u64::from_bytes(data[1]), 30);
                    }
                    _ => panic!("Expected entities {} or {} but was {}", eid_1, eid_3, id),
                }
            },
        );
        assert_eq!(called, 2);

        Ok(())
    }

    #[test]
    fn test_concurrency() -> Result<(), WaefError> {
        #![allow(arithmetic_overflow)]

        pub fn noise_u32(seed: u32, index: u32) -> u32 {
            let bit_noise1 = 0xb5297a4du32;
            let bit_noise2 = 0x68e31da4u32;
            let bit_noise3 = 0x1b56c4e9u32;

            let mut mangled = Wrapping(index);
            mangled *= bit_noise1;
            mangled += seed;
            mangled ^= mangled >> 8;
            mangled += bit_noise2;
            mangled ^= mangled << 8;
            mangled *= bit_noise3;
            mangled ^= mangled >> 8;
            mangled.0
        }

        let initial_entity_count: u32 = 50;
        let additional_entity_count: u32 = 50;
        let iteration_count: u32 = 5000;

        let (sender, _receiver) = channel();
        let main_ecs = WaefEcsRegistry::new_arc(sender);

        main_ecs.define_component_by_size("test_component_u8".into(), 1)?;
        main_ecs.define_component_by_size("test_component_u16".into(), 2)?;
        main_ecs.define_component_by_size("test_component_u32".into(), 4)?;
        main_ecs.define_component_by_size("test_component_u64".into(), 8)?;
        main_ecs.define_component_by_size("test_component_u128".into(), 16)?;

        let inc = AtomicU64::new(1);

        let ecs = main_ecs.clone();
        let create_entity = move || -> Result<(), WaefError> {
            let entity_id = inc.fetch_add(1, Ordering::AcqRel) as u128;
            ecs.create_entity(entity_id)?;
            _ = ecs.attach_component_buffer("test_component_u8".into(), entity_id, Box::new([1u8]));
            _ = ecs.attach_component_buffer(
                "test_component_u16".into(),
                entity_id,
                Box::new([1u8, 2u8]),
            );
            _ = ecs.attach_component_buffer(
                "test_component_u32".into(),
                entity_id,
                Box::new([1u8, 2u8, 3u8, 4u8]),
            );
            _ = ecs.attach_component_buffer(
                "test_component_u64".into(),
                entity_id,
                Box::new([1u8, 2u8, 3u8, 4u8, 5u8, 6u8, 7u8, 8u8]),
            );
            Ok(())
        };

        for _ in 0..initial_entity_count {
            create_entity()?;
        }

        let semaphore = Arc::new(Barrier::new(7));

        let threads = vec![
            {
                let barrier = semaphore.clone();
                thread::spawn(move || -> Result<u32, WaefError> {
                    barrier.wait();
                    for _ in 0..additional_entity_count {
                        create_entity()?;
                        std::thread::yield_now();
                    }
                    Ok(0)
                })
            },
            {
                let barrier = semaphore.clone();
                let ecs = main_ecs.clone();
                thread::spawn(move || -> Result<u32, WaefError> {
                    barrier.wait();
                    for _ in 0..iteration_count {
                        ecs.apply(
                            &["test_component_u8".into(), "test_component_u16".into()],
                            &mut |_, _| {
                                std::thread::yield_now();
                            },
                        );
                    }
                    Ok(1)
                })
            },
            {
                let barrier = semaphore.clone();
                let ecs = main_ecs.clone();
                thread::spawn(move || -> Result<u32, WaefError> {
                    barrier.wait();
                    for _ in 0..iteration_count {
                        ecs.apply(
                            &["test_component_u8".into(), "test_component_u16".into()],
                            &mut |_, _| {
                                ecs.apply(
                                    &["test_component_u8".into(), "test_component_u16".into()],
                                    &mut |_, _| {
                                        std::thread::yield_now();
                                    },
                                );
                                ecs.apply(&["test_component_u32".into()], &mut |_, _| {
                                    std::thread::yield_now();
                                });
                                std::thread::yield_now();
                            },
                        );
                    }
                    Ok(1)
                })
            },
            {
                let barrier = semaphore.clone();
                let ecs = main_ecs.clone();
                thread::spawn(move || -> Result<u32, WaefError> {
                    barrier.wait();
                    for _ in 0..iteration_count {
                        ecs.apply(
                            &["test_component_u16".into(), "test_component_u32".into()],
                            &mut |_, _| {
                                std::thread::yield_now();
                            },
                        );
                    }
                    Ok(2)
                })
            },
            {
                let barrier = semaphore.clone();
                let ecs = main_ecs.clone();
                thread::spawn(move || -> Result<u32, WaefError> {
                    barrier.wait();
                    for _ in 0..iteration_count {
                        ecs.apply(&["test_component_u64".into()], &mut |_, _| {
                            std::thread::yield_now();
                        });
                    }
                    Ok(3)
                })
            },
            {
                let barrier = semaphore.clone();
                let ecs = main_ecs.clone();
                thread::spawn(move || -> Result<u32, WaefError> {
                    barrier.wait();
                    for iteration in 0..iteration_count {
                        let mut j = 0;
                        let mut removes = Vec::with_capacity(256);
                        ecs.apply(&["test_component_u16".into()], &mut |eid, _| {
                            j = j + 1;
                            let do_remove = noise_u32(iteration, j) % 4 == 0;
                            if do_remove {
                                removes.push(eid);
                            }
                            std::thread::yield_now();
                        });
                        for entity_id in removes {
                            _ = ecs
                                .detach_component_by_name("test_component_u16".into(), entity_id);
                        }
                    }
                    Ok(4)
                })
            },
            {
                let barrier = semaphore.clone();
                let ecs = main_ecs.clone();
                thread::spawn(move || -> Result<u32, WaefError> {
                    barrier.wait();
                    for iteration in 0..iteration_count {
                        let mut j = 0;
                        let mut removes = Vec::with_capacity(256);
                        ecs.apply(&["test_component_u8".into()], &mut |eid, _| {
                            j = j + 1;
                            let do_remove = noise_u32(iteration + 1000, j) % 4 == 0;
                            if do_remove {
                                removes.push(eid);
                            }
                            std::thread::yield_now();
                        });
                        for entity_id in removes {
                            _ = ecs.destroy_entity(entity_id);
                        }
                    }
                    Ok(5)
                })
            },
        ];

        for thread in threads {
            _ = thread.join().unwrap()?;
        }
        Ok(())
    }

    #[test]
    fn test_benchmark() -> Result<(), WaefError> {
        let initial_entity_count: u32 = 50;
        let iteration_count: u32 = 100_000;

        let (sender, _receiver) = channel();
        let main_ecs = WaefEcsRegistry::new_arc(sender);

        main_ecs.define_component_by_size("test_component_u8".into(), 1)?;
        main_ecs.define_component_by_size("test_component_u16".into(), 2)?;
        main_ecs.define_component_by_size("test_component_u32".into(), 4)?;
        main_ecs.define_component_by_size("test_component_u64".into(), 8)?;
        main_ecs.define_component_by_size("test_component_u128".into(), 16)?;

        let inc = AtomicU64::new(1);

        let ecs = main_ecs.clone();
        let create_entity = move || -> Result<(), WaefError> {
            let entity_id = inc.fetch_add(1, Ordering::AcqRel) as u128;
            ecs.create_entity(entity_id)?;
            _ = ecs.attach_component_buffer("test_component_u8".into(), entity_id, Box::new([1u8]));
            _ = ecs.attach_component_buffer(
                "test_component_u16".into(),
                entity_id,
                Box::new([1u8, 2u8]),
            );
            _ = ecs.attach_component_buffer(
                "test_component_u32".into(),
                entity_id,
                Box::new([1u8, 2u8, 3u8, 4u8]),
            );
            _ = ecs.attach_component_buffer(
                "test_component_u64".into(),
                entity_id,
                Box::new([1u8, 2u8, 3u8, 4u8, 5u8, 6u8, 7u8, 8u8]),
            );
            Ok(())
        };

        for _ in 0..initial_entity_count {
            create_entity()?;
        }

        let ecs = main_ecs.clone();
        for _ in 0..iteration_count {
            ecs.apply(
                &["test_component_u8".into(), "test_component_u16".into()],
                &mut |_, ptrs| {
                    let mut u8v = u8::from_bytes(ptrs[0]);
                    u8v = (u8v + 1) % 128;
                    ptrs[0][0] = u8v;

                    let mut u16v = u16::from_bytes(ptrs[1]);
                    u16v = (u16v + 1) % 32768;
                    ptrs[1].copy_from_slice(u16v.try_split())
                },
            );
        }

        Ok(())
    }

    #[test]
    fn test_benchmark_physics() -> Result<(), WaefError> {
        const PARTICLE_COUNT: i32 = 300;
        const BALL_COUNT: i32 = 25;
        const BLOCK_COUNT: i32 = 25;
        const EXTRA_BALL_COUNT: i32 = 50;
        const ITERATION_COUNT: i32 = 6000;
        const TICK_TIME: f32 = 0.016f32;
        const GRAVITY: f32 = 1.0f32;

        struct Noise {}
        impl Noise {
            #![allow(arithmetic_overflow)]

            pub fn noise_u32(seed: u32, index: u32) -> u32 {
                let bit_noise1 = 0xb5297a4du32;
                let bit_noise2 = 0x68e31da4u32;
                let bit_noise3 = 0x1b56c4e9u32;

                let mut mangled = Wrapping(index);
                mangled *= bit_noise1;
                mangled += seed;
                mangled ^= mangled >> 8;
                mangled += bit_noise2;
                mangled ^= mangled << 8;
                mangled *= bit_noise3;
                mangled ^= mangled >> 8;
                mangled.0
            }

            pub fn noise_u16(seed: u32, index: u32) -> u16 {
                ((Self::noise_u32(seed, index) & 0x00ffff00u32) >> 8) as u16
            }

            // range [-1, 1]
            pub fn noise_f32(seed: u32, index: u32) -> f32 {
                (Self::noise_u16(seed, index) as f32) / 32767.5 - 1.0
            }
        }

        #[component("position")]
        struct Position {
            pub x: f32,
            pub y: f32,
        }

        #[component("velocity")]
        struct Velocity {
            pub x: f32,
            pub y: f32,
        }

        #[component("gravity_affected")]
        struct GravityAffected {}

        #[component("collision:circle")]
        struct CircleCollision {
            pub radius: f32,
        }

        #[component("collision:rect")]
        struct RectCollision {
            pub w: f32,
            pub h: f32,
        }

        let (sender, _receiver) = channel();
        let main_ecs = WaefEcsRegistry::new_arc(sender);

        let ecs = main_ecs.clone();
        ecs.define_component::<Position>()?;
        ecs.define_component::<Velocity>()?;
        ecs.define_component::<CircleCollision>()?;
        ecs.define_component::<RectCollision>()?;
        ecs.define_component::<GravityAffected>()?;

        for i in 0..PARTICLE_COUNT {
            let mut hash = DefaultHasher::new();
            hash.write_u32(
                std::time::SystemTime::now()
                    .duration_since(std::time::UNIX_EPOCH)
                    .unwrap()
                    .subsec_nanos(),
            );
            hash.write_i32(i);

            let seed = (hash.finish() % (u32::MAX as u64)) as u32;
            let entity_id = i as u128;
            ecs.create_entity_with(
                entity_id,
                (
                    Position {
                        x: Noise::noise_f32(seed, 0),
                        y: Noise::noise_f32(seed, 1),
                    },
                    Velocity {
                        x: Noise::noise_f32(seed, 3) * 0.1,
                        y: Noise::noise_f32(seed, 4) * 0.1,
                    },
                ),
            )?;
        }
        for i in 0..BALL_COUNT {
            let mut hash = DefaultHasher::new();
            hash.write_u32(
                std::time::SystemTime::now()
                    .duration_since(std::time::UNIX_EPOCH)
                    .unwrap()
                    .subsec_nanos(),
            );
            hash.write_i32(i);

            let seed = (hash.finish() % (u32::MAX as u64)) as u32;
            let entity_id = i as u128;
            ecs.create_entity_with(
                entity_id,
                (
                    Position {
                        x: Noise::noise_f32(seed, 0),
                        y: Noise::noise_f32(seed, 1),
                    },
                    Velocity {
                        x: Noise::noise_f32(seed, 3) * 0.1,
                        y: Noise::noise_f32(seed, 4) * 0.1,
                    },
                    CircleCollision {
                        radius: Noise::noise_f32(seed, 5).abs() * 9.0 + 1.0,
                    },
                    GravityAffected {},
                ),
            )?;
        }
        for i in 0..BLOCK_COUNT {
            let mut hash = DefaultHasher::new();
            hash.write_u32(
                std::time::SystemTime::now()
                    .duration_since(std::time::UNIX_EPOCH)
                    .unwrap()
                    .subsec_nanos(),
            );
            hash.write_i32(i);

            let seed = (hash.finish() % (u32::MAX as u64)) as u32;
            let entity_id = i as u128;
            ecs.create_entity_with(
                entity_id,
                (
                    Position {
                        x: Noise::noise_f32(seed, 0),
                        y: Noise::noise_f32(seed, 1),
                    },
                    Velocity {
                        x: Noise::noise_f32(seed, 3) * 0.1,
                        y: Noise::noise_f32(seed, 4) * 0.1,
                    },
                    RectCollision {
                        w: Noise::noise_f32(seed, 5).abs() * 24.0 + 1.0,
                        h: Noise::noise_f32(seed, 5).abs() * 4.0 + 1.0,
                    },
                ),
            )?;
        }

        let ecs = main_ecs.clone();
        let thread1 = std::thread::spawn(move || {
            for _ in 0..ITERATION_COUNT {
                ecs.query(
                    |_entity_id, (position, velocity): (&mut Position, &Velocity)| {
                        position.x += velocity.x * TICK_TIME;
                        position.y += velocity.y * TICK_TIME;
                    },
                );
            }
        });

        let ecs = main_ecs.clone();
        let thread2 = std::thread::spawn(move || {
            for _ in 0..ITERATION_COUNT {
                ecs.query(
                    |_entity_id, (velocity, _): (&mut Velocity, &GravityAffected)| {
                        velocity.y += GRAVITY * TICK_TIME;
                    },
                );
            }
        });

        let ecs = main_ecs.clone();
        let thread3 = std::thread::spawn(move || {
            for _ in 0..ITERATION_COUNT {
                let mut colliding = Vec::<(u128, u128)>::with_capacity(32);
                ecs.query(&mut |lhs,
                                (lhs_position, lhs_collider): (
                    &mut Position,
                    &CircleCollision,
                )| {
                    ecs.query(&mut |rhs,
                                    (rhs_position, rhs_collider): (
                        &mut Position,
                        &CircleCollision,
                    )| {
                        let delta_x = lhs_position.x - rhs_position.x;
                        let delta_y = lhs_position.y - rhs_position.y;
                        let delta_x_sq = delta_x * delta_x;
                        let delta_y_sq = delta_y * delta_y;
                        let mag_sq = delta_x_sq + delta_y_sq;
                        let len = lhs_collider.radius + rhs_collider.radius;
                        let len_sq = len * len;
                        if mag_sq < len_sq {
                            colliding.push((lhs, rhs));
                        }
                    });
                });
                if !colliding.is_empty() {
                    let set: Vec<u128> = colliding.iter().flat_map(|(l, r)| [*l, *r]).collect();
                    ecs.query_set(&set, |_, velocity: &mut Velocity| {
                        velocity.x = -velocity.x;
                    })
                }
            }
        });

        let ecs = main_ecs.clone();
        for i in 0..EXTRA_BALL_COUNT {
            let mut hash = DefaultHasher::new();
            hash.write_u32(
                std::time::SystemTime::now()
                    .duration_since(std::time::UNIX_EPOCH)
                    .unwrap()
                    .subsec_nanos(),
            );
            hash.write_i32(i);

            let seed = (hash.finish() % (u32::MAX as u64)) as u32;
            let entity_id = i as u128;
            ecs.create_entity_with(
                entity_id,
                (
                    Position {
                        x: Noise::noise_f32(seed, 0),
                        y: Noise::noise_f32(seed, 1),
                    },
                    Velocity {
                        x: Noise::noise_f32(seed, 3) * 0.1,
                        y: Noise::noise_f32(seed, 4) * 0.1,
                    },
                    CircleCollision {
                        radius: Noise::noise_f32(seed, 5).abs() * 9.0 + 1.0,
                    },
                    GravityAffected {},
                ),
            )?;
        }

        thread3.join().unwrap();
        thread2.join().unwrap();
        thread1.join().unwrap();

        Ok(())
    }
}

use std::{
    cell::RefCell,
    collections::{BTreeSet, HashMap, HashSet},
    ops::AddAssign,
    ptr::null_mut,
    sync::{mpsc::Sender, Arc},
};
use waef_core::{
    ecs::messages::{
        OnComponentDefined, OnEntityComponentAttached, OnEntityComponentDetached, OnEntityCreated,
        OnEntityDestroyed,
    },
    EcsRegistry, IsMessage, Message, WaefError,
};

use crate::util::{archetype_store::ArchetypeStore, registry::ArchetypeRegistry};

#[derive(Default)]
struct ComponentQuery {
    exclude: BTreeSet<u32>,
    include: BTreeSet<u32>,
    optional: BTreeSet<u32>,
}

#[derive(Default)]
struct ComponentQueryDefinitions {
    component_ids: Vec<u32>,
    component_sizes: Vec<usize>,
}

pub struct WaefEcsRegistry {
    sender: Sender<Message>,

    next_component_id: RefCell<u32>,
    component_definitions: RefCell<HashMap<String, (usize, u32)>>,
    component_keys: RefCell<HashMap<u32, String>>,
    archetype_registry: RefCell<ArchetypeRegistry<u32>>,
}
impl WaefEcsRegistry {
    pub fn new(dispatcher: Sender<Message>) -> Self {
        Self {
            sender: dispatcher,
            next_component_id: RefCell::new(1),
            component_definitions: RefCell::new(HashMap::with_capacity(32)),
            component_keys: RefCell::new(HashMap::with_capacity(32)),
            archetype_registry: RefCell::new(ArchetypeRegistry::<u32>::with_capacity(1024, 32, 64)),
        }
    }

    pub fn new_arc(dispatcher: Sender<Message>) -> Arc<dyn EcsRegistry> {
        Arc::new(Self::new(dispatcher))
    }

    fn next_component_id(&self) -> u32 {
        let mut next_component_id = self.next_component_id.borrow_mut();

        let result = *next_component_id;
        next_component_id.add_assign(1);
        result
    }

    fn get_or_create_component_id(&self, component_name: &String, size: usize) -> u32 {
        let mut component_definitions = self.component_definitions.borrow_mut();

        if let Some(id) = component_definitions
            .get(component_name)
            .cloned()
            .map(|(_, id)| id)
        {
            id
        } else {
            let mut component_keys = self.component_keys.borrow_mut();

            let id = self.next_component_id();
            component_definitions.insert(component_name.clone(), (size, id));
            component_keys.insert(id, component_name.clone());
            id
        }
    }

    fn get_component_id(&self, component_name: &String) -> Option<u32> {
        let component_definitions = self.component_definitions.borrow();

        component_definitions
            .get(component_name)
            .cloned()
            .map(|(_, id)| id)
    }

    fn parse_query(
        &self,
        component_query: &[String],
    ) -> (ComponentQuery, ComponentQueryDefinitions) {
        let component_definitions = self.component_definitions.borrow();

        let mut query = ComponentQuery::default();
        let mut query_definitions = ComponentQueryDefinitions::default();
        component_query.into_iter().for_each(|component_name| {
            match component_name.chars().nth(0) {
                Some('!') => {
                    let component_name = component_name[1..].to_string();
                    let def = component_definitions
                        .get(&component_name)
                        .cloned()
                        .unwrap_or((0, 0));

                    query.exclude.insert(def.1);
                }
                Some('?') => {
                    let component_name = component_name[1..].to_string();
                    let def = component_definitions
                        .get(&component_name)
                        .cloned()
                        .unwrap_or((0, 0));

                    query.optional.insert(def.1);
                    query_definitions.component_sizes.push(def.0);
                    query_definitions.component_ids.push(def.1);
                }
                Some(_) => {
                    let def = component_definitions
                        .get(component_name)
                        .cloned()
                        .unwrap_or((0, 0));

                    query.include.insert(def.1);
                    query_definitions.component_sizes.push(def.0);
                    query_definitions.component_ids.push(def.1);
                }
                None => {}
            }
        });
        (query, query_definitions)
    }

    fn apply_archetype_store(
        &self,
        component_query: &[String],
        callback: &mut dyn FnMut(&ComponentQueryDefinitions, &ArchetypeStore<u32>),
    ) {
        let (component_query, component_query_definitions) = self.parse_query(component_query);

        self.archetype_registry
            .borrow()
            .query(component_query.include, component_query.exclude)
            .for_each(|store| callback(&component_query_definitions, store))
    }
}
unsafe impl Sync for WaefEcsRegistry {}
unsafe impl Send for WaefEcsRegistry {}

impl EcsRegistry for WaefEcsRegistry {
    fn list_component_definitions(&self) -> HashMap<String, usize> {
        let _guard = tracing::trace_span!("ecs::list_component_definitions").entered();

        self.component_definitions
            .borrow()
            .iter()
            .map(|(k, (size, _))| (k.clone(), *size))
            .collect()
    }

    fn define_component_by_size(&self, name: String, size: usize) -> Result<(), WaefError> {
        let _guard = tracing::trace_span!(
            "ecs::define_component_by_size",
            component_name = name,
            component_size = size
        )
        .entered();

        let mut component_definitions = self.component_definitions.borrow_mut();
        if component_definitions.contains_key(&name) {
            Err(WaefError::new("component already defined"))
        } else {
            let mut component_keys = self.component_keys.borrow_mut();

            let id = self.next_component_id();
            component_definitions.insert(name.clone(), (size, id));
            component_keys.insert(id, name.clone());

            _ = self.sender.send(
                OnComponentDefined {
                    component_name: name,
                    component_size: size as u32,
                }
                .into_message(),
            );
            Ok(())
        }
    }

    fn get_component_size(&self, name: String) -> Option<usize> {
        let _guard =
            tracing::trace_span!("ecs::get_component_size", component_name = name).entered();

        self.component_definitions
            .borrow()
            .get(&name)
            .map(|(size, _)| size)
            .cloned()
    }

    fn create_entity(&self, entity_id: u128) -> Result<(), WaefError> {
        let _guard = tracing::trace_span!("ecs::create_entity", entity_id).entered();

        self.archetype_registry
            .borrow_mut()
            .insert_entity(entity_id, HashMap::new());

        self.sender
            .send(OnEntityCreated { entity_id }.into_message())
            .map_err(WaefError::new)?;

        Ok(())
    }

    fn destroy_entity(&self, entity_id: u128) -> Result<(), WaefError> {
        let _guard = tracing::trace_span!("ecs::destroy_entity", entity_id).entered();

        if let Some(entity_components) = self
            .archetype_registry
            .borrow_mut()
            .remove_entity(entity_id)
        {
            let component_keys = self.component_keys.borrow();
            for component_name in entity_components
                .keys()
                .flat_map(|key| component_keys.get(key))
                .cloned()
            {
                self.sender
                    .send(
                        OnEntityComponentDetached {
                            entity_id,
                            component_name,
                        }
                        .into_message(),
                    )
                    .map_err(WaefError::new)?;
            }
            self.sender
                .send(OnEntityDestroyed { entity_id }.into_message())
                .map_err(WaefError::new)?;

            Ok(())
        } else {
            Err(WaefError::new("entity not found"))
        }
    }

    fn attach_component_buffer(
        &self,
        component_name: String,
        entity_id: u128,
        component_buffer: Box<[u8]>,
    ) -> Result<(), WaefError> {
        let _guard = tracing::trace_span!("ecs::attach_component_buffer", entity_id, component_name)
            .entered();

        let component_id = self.get_or_create_component_id(&component_name, component_buffer.len());

        let mut archetype_registry = self.archetype_registry.borrow_mut();
        if let Some(mut components) = archetype_registry.remove_entity(entity_id) {
            components.insert(component_id, component_buffer);
            archetype_registry.insert_entity(entity_id, components);

            self.sender
                .send(
                    OnEntityComponentAttached {
                        entity_id,
                        component_name,
                    }
                    .into_message(),
                )
                .map_err(WaefError::new)?;

            Ok(())
        } else {
            Err(WaefError::new("entity not found"))
        }
    }

    fn detach_component_by_name(
        &self,
        component_name: String,
        entity_id: u128,
    ) -> Result<(), WaefError> {
        let _guard =
            tracing::trace_span!("ecs::detach_component_by_name", entity_id, component_name)
                .entered();

        if let Some(component_id) = self.get_component_id(&component_name) {
            let mut archetype_registry = self.archetype_registry.borrow_mut();
            if let Some(mut components) = archetype_registry.remove_entity(entity_id) {
                components.remove(&component_id);
                archetype_registry.insert_entity(entity_id, components);

                self.sender
                    .send(
                        OnEntityComponentDetached {
                            entity_id,
                            component_name,
                        }
                        .into_message(),
                    )
                    .map_err(WaefError::new)?;
                Ok(())
            } else {
                Err(WaefError::new("entity not found"))
            }
        } else {
            Err(WaefError::new("component not found"))
        }
    }

    fn apply(&self, component_query: &[String], callback: &mut dyn FnMut(u128, &mut [&mut [u8]])) {
        let _guard = tracing::trace_span!("ecs::apply", component_query = component_query.join(" "))
            .entered();

        let mut data_store = Vec::with_capacity(component_query.len());
        self.apply_unsafe(component_query, &mut |entity_id, entity_ptrs| {
            let max_buffer_size = entity_ptrs.iter().map(|x| x.1).max().unwrap_or(32usize);
            let mut empty_buffer = vec![0u8; max_buffer_size];

            data_store.clear();
            data_store.extend(entity_ptrs.iter().map(|items| {
                let ptr = items.0;
                let size = items.1;
                if ptr.is_null() {
                    unsafe { std::slice::from_raw_parts_mut(empty_buffer.as_mut_ptr(), size) }
                } else {
                    unsafe { std::slice::from_raw_parts_mut(ptr, size) }
                }
            }));
            callback(entity_id, &mut data_store[..]);
        });
    }

    fn apply_set(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_set",
            entity_count = entity_ids.len(),
            component_query = component_query.join(" ")
        )
        .entered();

        let mut data_store = Vec::with_capacity(component_query.len());
        self.apply_set_unsafe(
            entity_ids,
            component_query,
            &mut |entity_id, entity_ptrs| {
                let max_buffer_size = entity_ptrs.iter().map(|x| x.1).max().unwrap_or(32usize);
                let mut empty_buffer = vec![0u8; max_buffer_size];

                data_store.clear();
                data_store.extend(entity_ptrs.iter().map(|items| {
                    let ptr = items.0;
                    let size = items.1;
                    if ptr.is_null() {
                        unsafe { std::slice::from_raw_parts_mut(empty_buffer.as_mut_ptr(), size) }
                    } else {
                        unsafe { std::slice::from_raw_parts_mut(ptr, size) }
                    }
                }));
                callback(entity_id, &mut data_store[..]);
            },
        );
    }

    fn apply_single(
        &self,
        entity_id: u128,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &mut [&mut [u8]]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_single",
            entity_id = entity_id,
            component_query = component_query.join(" ")
        )
        .entered();

        let mut data_store = Vec::with_capacity(component_query.len());
        self.apply_single_unsafe(entity_id, component_query, &mut |entity_id, entity_ptrs| {
            let max_buffer_size = entity_ptrs.iter().map(|x| x.1).max().unwrap_or(32usize);
            let mut empty_buffer = vec![0u8; max_buffer_size];

            data_store.clear();
            data_store.extend(entity_ptrs.iter().map(|items| {
                let ptr = items.0;
                let size = items.1;
                if ptr.is_null() {
                    unsafe { std::slice::from_raw_parts_mut(empty_buffer.as_mut_ptr(), size) }
                } else {
                    unsafe { std::slice::from_raw_parts_mut(ptr, size) }
                }
            }));
            callback(entity_id, &mut data_store[..]);
        });
    }

    fn apply_set_unsafe(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_set_unsafe",
            entity_count = entity_ids.len(),
            component_query = component_query.join(" ")
        )
        .entered();

        let entity_id_set: HashSet<u128> = HashSet::from_iter(entity_ids.iter().cloned());
        self.apply_archetype_store(
            component_query,
            &mut |component_query_definitions: &ComponentQueryDefinitions,
                  store: &ArchetypeStore<u32>| unsafe {
                let mut component_heaps: Vec<_> = store
                    .component_heap_indices(component_query_definitions.component_ids.iter())
                    .map(|index| match index {
                        Some(index) => Some(store.component_heap_at_index_unsafe(index)),
                        None => None,
                    })
                    .enumerate()
                    .collect();

                let mut callback_data: Vec<(*mut u8, usize)> =
                    vec![(null_mut(), 0); component_query_definitions.component_ids.len()];
                store.entity_heap_indices().for_each(|heap_index| {
                    let entity_id = store.entity_id_at_index(heap_index);
                    if !entity_id_set.contains(&entity_id) {
                        return;
                    }

                    component_heaps
                        .iter_mut()
                        .for_each(|(callback_index, heap)| match heap {
                            Some(heap) => {
                                callback_data[*callback_index] = (
                                    heap[heap_index].as_mut_ptr(),
                                    component_query_definitions.component_sizes[*callback_index],
                                );
                            }
                            None => {
                                callback_data[*callback_index] = (
                                    null_mut(),
                                    component_query_definitions.component_sizes[*callback_index],
                                );
                            }
                        });
                    callback(entity_id, &callback_data);
                })
            },
        )
    }

    fn apply_unsafe(
        &self,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_unsafe",
            component_query = component_query.join(" ")
        )
        .entered();

        self.apply_unsafe_batched(component_query, &mut |items| {
            items
                .iter()
                .for_each(|(entity_id, items)| callback(*entity_id, *items))
        });
    }

    fn apply_single_unsafe(
        &self,
        entity_id: u128,
        component_query: &[String],
        callback: &mut dyn FnMut(u128, &[(*mut u8, usize)]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_single_unsafe",
            entity_id = entity_id,
            component_query = component_query.join(" ")
        )
        .entered();

        self.apply_archetype_store(
            component_query,
            &mut |component_query_definitions: &ComponentQueryDefinitions,
                  store: &ArchetypeStore<u32>| unsafe {
                let mut component_heaps: Vec<_> = store
                    .component_heap_indices(component_query_definitions.component_ids.iter())
                    .map(|index| match index {
                        Some(index) => Some(store.component_heap_at_index_unsafe(index)),
                        None => None,
                    })
                    .enumerate()
                    .collect();

                let mut callback_data: Vec<(*mut u8, usize)> =
                    vec![(null_mut(), 0); component_query_definitions.component_ids.len()];
                if let Some(heap_index) = store.entity_heap_index_for_entity_id(entity_id) {
                    component_heaps
                        .iter_mut()
                        .for_each(|(callback_index, heap)| match heap {
                            Some(heap) => {
                                callback_data[*callback_index] = (
                                    heap[heap_index].as_mut_ptr(),
                                    component_query_definitions.component_sizes[*callback_index],
                                );
                            }
                            None => {
                                callback_data[*callback_index] = (
                                    null_mut(),
                                    component_query_definitions.component_sizes[*callback_index],
                                );
                            }
                        });
                    callback(entity_id, &callback_data);
                }
            },
        )
    }

    fn apply_unsafe_batched(
        &self,
        component_query: &[String],
        callback: &mut dyn FnMut(&[(u128, &[(*mut u8, usize)])]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_unsafe_batched",
            component_query = component_query.join(" ")
        )
        .entered();

        self.apply_archetype_store(
            component_query,
            &mut |component_query_definitions: &ComponentQueryDefinitions,
                  store: &ArchetypeStore<u32>| unsafe {
                let mut component_heaps: Vec<_> = store
                    .component_heap_indices(component_query_definitions.component_ids.iter())
                    .map(|index| match index {
                        Some(index) => Some(store.component_heap_at_index_unsafe(index)),
                        None => None,
                    })
                    .enumerate()
                    .collect();

                let mut callback_data: Vec<(*mut u8, usize)> =
                    Vec::with_capacity(store.len() * component_heaps.len());
                let mut entity_callback_data: Vec<(u128, &[(*mut u8, usize)])> =
                    Vec::with_capacity(store.len());

                store.entity_heap_indices().for_each(&mut |heap_index| {
                    let entity_id = store.entity_id_at_index(heap_index);
                    component_heaps
                        .iter_mut()
                        .for_each(|(callback_index, heap)| match heap {
                            Some(heap) => {
                                callback_data.push((
                                    heap[heap_index].as_mut_ptr(),
                                    component_query_definitions.component_sizes[*callback_index],
                                ));
                            }
                            None => {
                                callback_data.push((
                                    null_mut(),
                                    component_query_definitions.component_sizes[*callback_index],
                                ));
                            }
                        });
                    entity_callback_data.push((entity_id, &[]));
                });
                entity_callback_data
                    .iter_mut()
                    .enumerate()
                    .for_each(|(index, result)| {
                        let head = index * component_heaps.len();
                        let tail = head + component_heaps.len();
                        result.1 = &callback_data[head..tail];
                    });
                callback(&entity_callback_data);
            },
        )
    }

    fn apply_set_unsafe_batched(
        &self,
        entity_ids: &[u128],
        component_query: &[String],
        callback: &mut dyn FnMut(&[(u128, &[(*mut u8, usize)])]),
    ) {
        let _guard = tracing::trace_span!(
            "ecs::apply_set_unsafe_batched",
            entity_count = entity_ids.len(),
            component_query = component_query.join(" ")
        )
        .entered();
    
        let entity_id_set: HashSet<u128> = HashSet::from_iter(entity_ids.iter().cloned());
        self.apply_archetype_store(
            component_query,
            &mut |component_query_definitions: &ComponentQueryDefinitions,
                  store: &ArchetypeStore<u32>| unsafe {
                let mut component_heaps: Vec<_> = store
                    .component_heap_indices(component_query_definitions.component_ids.iter())
                    .map(|index| match index {
                        Some(index) => Some(store.component_heap_at_index_unsafe(index)),
                        None => None,
                    })
                    .enumerate()
                    .collect();

                let mut callback_data: Vec<(*mut u8, usize)> =
                    Vec::with_capacity(entity_ids.len() * component_heaps.len());
                let mut entity_callback_data: Vec<(u128, &[(*mut u8, usize)])> =
                    Vec::with_capacity(entity_ids.len());

                store.entity_heap_indices().for_each(&mut |heap_index| {
                    let entity_id = store.entity_id_at_index(heap_index);
                    if !entity_id_set.contains(&entity_id) {
                        return;
                    }

                    component_heaps
                        .iter_mut()
                        .for_each(|(callback_index, heap)| match heap {
                            Some(heap) => {
                                callback_data.push((
                                    heap[heap_index].as_mut_ptr(),
                                    component_query_definitions.component_sizes[*callback_index],
                                ));
                            }
                            None => {
                                callback_data.push((
                                    null_mut(),
                                    component_query_definitions.component_sizes[*callback_index],
                                ));
                            }
                        });
                    entity_callback_data.push((entity_id, &[]));
                });
                entity_callback_data
                    .iter_mut()
                    .enumerate()
                    .for_each(|(index, result)| {
                        let head = index * component_heaps.len();
                        let tail = head + component_heaps.len();
                        result.1 = &callback_data[head..tail];
                    });
                callback(&entity_callback_data);
            },
        )
    }
}

#[cfg(test)]
mod tests {
    use std::sync::mpsc::channel;

    use super::WaefEcsRegistry;
    use waef_core::core::WaefError;
    use waef_core::util::pod::Pod;

    #[test]
    pub fn test_ecs() -> Result<(), WaefError> {
        let (sender, _receiver) = channel();

        let ecs = WaefEcsRegistry::new_arc(sender);
        ecs.define_component_by_size("test_component_1".into(), 4)?;
        ecs.define_component_by_size("test_component_2".into(), 8)?;
        ecs.define_component_by_size("test_tag_1".into(), 0)?;

        assert_eq!(ecs.get_component_size("test_component_1".into()), Some(4));
        assert_eq!(ecs.get_component_size("test_component_2".into()), Some(8));
        assert_eq!(ecs.get_component_size("test_component_noop".into()), None);

        let eid_1: u128 = 1;
        ecs.create_entity(eid_1)?;
        ecs.attach_component_buffer("test_component_1".into(), eid_1, 10_u32.into_boxed_slice())?;
        ecs.attach_component_buffer("test_component_2".into(), eid_1, 25_u64.into_boxed_slice())?;
        ecs.attach_component_buffer("test_tag_1".into(), eid_1, Box::new([]))?;
        ecs.detach_component_by_name("test_component_2".into(), eid_1)?;

        let eid_2: u128 = 2;
        ecs.create_entity(eid_2)?;
        ecs.attach_component_buffer("test_component_2".into(), eid_2, 15_u64.into_boxed_slice())?;
        ecs.attach_component_buffer("test_tag_1".into(), eid_2, Box::new([]))?;

        let eid_3: u128 = 3;
        ecs.create_entity(eid_3)?;
        ecs.attach_component_buffer("test_component_1".into(), eid_3, 20_u32.into_boxed_slice())?;
        ecs.attach_component_buffer("test_component_2".into(), eid_3, 30_u64.into_boxed_slice())?;

        let eid_4: u128 = 4;
        ecs.create_entity(eid_4)?;
        ecs.attach_component_buffer("test_component_1".into(), eid_4, 123_u32.into_boxed_slice())?;
        ecs.attach_component_buffer("test_component_2".into(), eid_4, 456_u64.into_boxed_slice())?;
        ecs.attach_component_buffer("test_tag_1".into(), eid_4, Box::new([]))?;
        ecs.destroy_entity(eid_4)?;

        let mut called = 0;
        ecs.apply(&["test_component_1".into()], &mut |id, data| {
            called = called + 1;

            assert_eq!(data.len(), 1);
            assert_eq!(data[0].len(), 4);

            match id {
                _ if id == eid_1 => {
                    assert_eq!(u32::from_bytes(data[0]), 10);
                    data[0].copy_from_slice(11_u32.try_split());
                }
                _ if id == eid_3 => assert_eq!(u32::from_bytes(data[0]), 20),
                _ => panic!("Expected entities {} or {} but was {}", eid_1, eid_3, id),
            }
        });
        assert_eq!(called, 2);

        let mut called = 0;
        ecs.apply(&["test_component_1".into()], &mut |id, data| {
            called = called + 1;
            assert_eq!(data.len(), 1);
            assert_eq!(data[0].len(), 4);

            match id {
                _ if id == eid_1 => assert_eq!(u32::from_bytes(data[0]), 11),
                _ if id == eid_3 => assert_eq!(u32::from_bytes(data[0]), 20),
                _ => panic!("Expected entities {} or {} but was {}", eid_1, eid_3, id),
            }
        });
        assert_eq!(called, 2);

        let mut called = 0;
        ecs.apply(&["test_tag_1".into()], &mut |id, data| {
            called = called + 1;
            assert_eq!(data.len(), 1);
            assert_eq!(data[0].len(), 0);

            match id {
                _ if id == eid_1 => (),
                _ if id == eid_2 => (),
                _ => panic!("Expected entities {} or {} but was {}", eid_1, eid_2, id),
            }
        });
        assert_eq!(called, 2);

        let mut called = 0;
        ecs.apply_single(eid_1, &["test_tag_1".into()], &mut |id, data| {
            called = called + 1;
            assert_eq!(data.len(), 1);
            assert_eq!(data[0].len(), 0);

            match id {
                _ if id == eid_1 => (),
                _ => panic!("Expected entity {} but was {}", eid_1, id),
            }
        });
        assert_eq!(called, 1);

        let mut called = 0;
        ecs.apply(
            &["test_component_1".into(), "test_component_2".into()],
            &mut |id, data| {
                called = called + 1;
                assert_eq!(data.len(), 2);
                assert_eq!(data[0].len(), 4);
                assert_eq!(data[1].len(), 8);

                match id {
                    _ if id == eid_3 => {
                        assert_eq!(u32::from_bytes(data[0]), 20);
                        assert_eq!(u64::from_bytes(data[1]), 30);
                    }
                    _ => panic!("Expected entity {} but was {}", eid_3, id),
                }
            },
        );
        assert_eq!(called, 1);

        let mut called = 0;
        ecs.apply(
            &["test_component_1".into(), "!test_component_2".into()],
            &mut |id, data| {
                called = called + 1;
                assert_eq!(data.len(), 1);

                match id {
                    _ if id == eid_1 => {
                        assert_eq!(u32::from_bytes(data[0]), 11);
                        data[0].copy_from_slice(12_u32.try_split());
                    }
                    _ => panic!("Expected entities {} or {} but was {}", eid_1, eid_3, id),
                }
            },
        );
        assert_eq!(called, 1);

        let mut called = 0;
        ecs.apply(
            &["test_component_1".into(), "test_component_2".into()],
            &mut |_id, _data| {
                called = called + 1;
                _ = ecs.apply(
                    &["test_component_1".into(), "test_component_2".into()],
                    &mut |id, data| {
                        called = called + 1;
                        assert_eq!(data.len(), 2);
                        assert_eq!(data[0].len(), 4);
                        assert_eq!(data[1].len(), 8);

                        match id {
                            _ if id == eid_3 => {
                                assert_eq!(u32::from_bytes(data[0]), 20);
                                assert_eq!(u64::from_bytes(data[1]), 30);
                            }
                            _ => panic!("Expected entity {} but was {}", eid_3, id),
                        }
                    },
                );
            },
        );
        assert_eq!(called, 2);

        let mut called = 0;
        ecs.apply(
            &["test_component_1".into(), "?test_component_2".into()],
            &mut |id, data| {
                called = called + 1;
                assert_eq!(data.len(), 2);
                assert_eq!(data[0].len(), 4);
                assert_eq!(data[1].len(), 8);

                match id {
                    _ if id == eid_1 => {
                        assert_eq!(u32::from_bytes(data[0]), 12);
                        assert_eq!(u64::from_bytes(data[1]), 0);
                    }
                    _ if id == eid_3 => {
                        assert_eq!(u32::from_bytes(data[0]), 20);
                        assert_eq!(u64::from_bytes(data[1]), 30);
                    }
                    _ => panic!("Expected entities {} or {} but was {}", eid_1, eid_3, id),
                }
            },
        );
        assert_eq!(called, 2);

        Ok(())
    }
}
